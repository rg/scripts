#!/usr/bin/env bash
set -xe

URL="https://github.com/$(curl -s https://github.com/terraform-docs/terraform-docs/releases | grep download | grep linux-amd64 | head -n1 | cut -d'"' -f2)"
sudo curl -s -o /usr/local/bin/terraform-docs -L $URL
sudo chmod +x /usr/local/bin/terraform-docs
terraform-docs --version
