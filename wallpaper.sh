#!/bin/bash

# Script to be put in /etc/cron.hourly/wallpaper.sh
# Adjust search, width and height as u see fit
# Don't forget chmod +x

which unsplash-wallpaper &>/dev/null
if [ ! $? ]; then
  which npm
  if [ $? ]; then
    npm install -g unsplash-wallpaper
  else
    echo "unsplash-wallpaper missing, and npm too, exiting"
    exit 1
  fi
fi

unsplash-wallpaper -f --search mountain --width 1920 --height 1080 &>/dev/null
