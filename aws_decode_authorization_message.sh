#!/usr/bin/env bash

MESSAGE=$1

aws sts decode-authorization-message --encoded-message "${MESSAGE}" | jq -r '.DecodedMessage' | sed 's;\";";' | jq '.'
