#!/usr/bin/env bash

sudo dra download -s iam-policy-json-to-terraform_amd64 flosell/iam-policy-json-to-terraform -o /usr/local/bin/aws_iam_policy_json_to_terraform_statement
sudo chmod +x /usr/local/bin/aws_iam_policy_json_to_terraform_statement
