#!/usr/bin/env bash

FILTER=${1:-""}

for p in $(aws ssm describe-parameters | jq -r ".Parameters[]|select(.Name|test(\"^$FILTER.*\"))|.Name") ; do echo  "$p : " ; aws ssm get-parameter --name $p --with-decryption  ; done;
