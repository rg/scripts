#!/usr/bin/env bash
set -euo pipefail

VERSION=$(curl -s https://api.github.com/repos/synfinatic/aws-sso-cli/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | tail -n1)
curl --progress-bar -LO "https://github.com/synfinatic/aws-sso-cli/releases/download/$VERSION/aws-sso-cli_${VERSION/v/}-1_amd64.deb"
sudo apt-get install -y -q ./aws-sso-cli*.deb
rm ./aws-sso-cli*.deb
echo
aws-sso version
