#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/nektos/act/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)
curl -LO https://github.com/nektos/act/releases/download/$VERSION/act_Linux_x86_64.tar.gz
tar -xf act*.tar.gz act
sudo mv act /usr/local/bin/
rm -rf act*.tar.gz
cat <<EOF >~/.actrc
# Configuration file for nektos/act
# https://github.com/nektos/act#flags
-P ubuntu-latest=nektos/act-environments-ubuntu:18.04
EOF
