#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Get minion's users informations as saltstack custom grains "users"
"""


def users_grains():
    grains = {"users": {}}

    with open("/etc/passwd") as file:
        for line in file:
            user = line.split(":")
            grains["users"][user[0]] = {
                "uid": user[2],
                "gid": user[3],
                "home": user[5],
                "shell": user[6],
            }

    return grains
