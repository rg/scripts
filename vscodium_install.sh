#!/usr/bin/env bash

# Microsoft VSCode
# sudo snap install code --classic
# for extension in alefragnani.project-manager bbenoist.vagrant bungcip.better-toml christian-kohler.path-intellisense codezombiech.gitignore coolbear.systemd-unit-file eamodio.gitlens EditorConfig.EditorConfig emilast.LogFileHighlighter foxundermoon.shell-format HookyQR.beautify johnpapa.vscode-peacock karigari.chat lextudio.restructuredtext mauve.terraform mechatroner.rainbow-csv ms-azuretools.vscode-docker ms-python.python ms-vscode-remote.remote-ssh ms-vscode-remote.remote-ssh-edit ms-vsliveshare.vsliveshare ms-vsliveshare.vsliveshare-audio ms-vsliveshare.vsliveshare-pack redhat.vscode-yaml Shan.code-settings-sync slevesque.vscode-zipexplorer timonwong.shellcheck tomoki1207.pdf wholroyd.jinja yzane.markdown-pdf; do
# code --install-extension "$extension"
# done
# cp "$WKDIR/vscode_settings.json" "$HOME/.config/Code/User/settings.json"

# Community VSCodium
snap install codium --classic

# codium --list-extensions | tr '\n' ' ' | clipcopy
for extension in christian-kohler.path-intellisense cschleiden.vscode-github-actions eamodio.gitlens EditorConfig.EditorConfig esbenp.prettier-vscode foxundermoon.shell-format hashicorp.terraform ms-python.python PKief.material-icon-theme redhat.vscode-yaml samuelcolvin.jinjahtml timonwong.shellcheck yzhang.markdown-all-in-one; do
  codium --install-extension "$extension"
done

cp "$WKDIR/vscodium_settings.json" "$HOME/.config/VSCodium/User/settings.json"
cp "$WKDIR/vscodium_product.json" "$HOME/.config/VSCodium/product.json"

# Allow watching more files
# https://code.visualstudio.com/docs/setup/linux#_visual-studio-code-is-unable-to-watch-for-file-changes-in-this-large-workspace-error-enospc
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf
sudo sysctl fs.inotify.max_user_watches=524288
