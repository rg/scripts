#!/usr/bin/env bash
set -euo pipefail

ORG="FiloSottile"
APP="age"
URL="https://github.com/$(curl -s https://github.com/${ORG}/${APP}/releases | grep download | grep linux-amd64 | head -n1 | cut -d'"' -f2)"
curl -sL "${URL}" | sudo tar -xz -C /usr/local/bin --strip-components=1 age/age age/age-keygen

"/usr/local/bin/$APP" --version
