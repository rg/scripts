#!/usr/bin/env bash

VERSION=1.14.1
curl https://dl.google.com/go/go$VERSION.linux-amd64.tar.gz -O go.tar.gz
tar -C /usr/local/bin -xzf go.tar.gz
rm -f go.tar.gz
