#!/usr/bin/env bash

apt update ; apt install -y less unzip curl sudo ; curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" ; unzip awscliv2.zip ; sudo ./aws/install --update ; rm -rf aws awscliv2.zip ; aws --version ; 
