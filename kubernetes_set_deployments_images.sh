#!/usr/bin/env bash

DEPLOYMENTS=$HOME/work/gostudent/infrastructure/deployments/product-prod/services

for service in $(cat images.csv)
do
  name=$(echo $service | cut -d ';' -f 1)
  revision=$(echo $service | cut -d ';' -f 2)
  file=$DEPLOYMENTS/$name/config.yaml 
  if [ -f $file ]
  then
    if ! grep -q $revision $file
    then
      echo "$name revision set to $revision"
      sed -i "s;revision:.*;revision: $revision;" $file
    else
      echo "$name revision already set"
    fi
  else
    echo "$name missing file $file" 
  fi
done

