#!/usr/bin/env bash

curl -L https://istio.io/downloadIstio | sh -
sudo mv istio-*/bin/istioctl /usr/local/bin
rm -rf istio-*
istioctl version
