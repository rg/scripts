#!/usr/bin/env python
import smtplib
import typer
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

app = typer.Typer()

@app.command()
def send_email(
    sender: str = typer.Option(..., help="The sender's email address."),
    recipient: str = typer.Option(..., help="The recipient's email address."),
    subject: str = typer.Option(..., help="The subject of the email."),
    body_text: str = typer.Option(..., help="The body text of the email."),
    smtp_username: str = typer.Option(..., help="Your SMTP username (AWS SES SMTP credentials)."),
    smtp_password: str = typer.Option(..., help="Your SMTP password (AWS SES SMTP credentials)."),
    smtp_host: str = typer.Option("email-smtp.eu-north-1.amazonaws.com", help="The SMTP endpoint for AWS SES."),
    smtp_port: int = typer.Option(587, help="The SMTP port (587 for TLS, 465 for SSL)."),
):
    """Send an email using AWS SES SMTP credentials."""
    
    # Create the email content
    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = recipient
    msg['Subject'] = subject

    # Attach the body text
    msg.attach(MIMEText(body_text, 'plain'))

    try:
        # Connect to the SMTP server
        with smtplib.SMTP(smtp_host, smtp_port) as server:
            server.starttls()  # Upgrade the connection to a secure encrypted SSL/TLS connection
            server.login(smtp_username, smtp_password)  # Log in to the SMTP server
            server.sendmail(sender, recipient, msg.as_string())  # Send the email

        typer.echo("Email sent successfully!")
    except Exception as e:
        typer.echo(f"Failed to send email: {str(e)}")

if __name__ == "__main__":
    app()
