#!/bin/bash

# Usage:
#   vscode_folder_as_project.sh > ~/.config/Code/User/globalStorage/alefragnani.project-manager/projects.json

JSON=$(
  echo "["
  for ROOTPATH in $(find $HOME -name .git 2>/dev/null | sed 's;/.git;;' | egrep -v '\.terraform|\.terragrunt-cache|\.cache|\.bundle|\.pyenv|src|\.local|\.vscode'); do
    cd $ROOTPATH || exit
    NAME=${PWD##*/}
    echo "  {"
    echo "    \"name\": \"$NAME\","
    echo "    \"rootPath\": \"$ROOTPATH\","
    echo "    \"paths\": [],"
    echo "    \"group\": \"\","
    echo "    \"enabled\": true"
    echo "  },"
    cd - &>/dev/null || exit
  done
  echo "]"
)

echo "$JSON" | tr -d '\n' | sed 's;\(.*\),;\1;'
