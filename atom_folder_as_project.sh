#!/bin/bash

echo "["
for PATH in $(find "$HOME" -name .git 2>/dev/null | sed 's;/.git;;' | grep -E -v '\.terraform|\.cache|\.bundle|\.pyenv|src|\.local|\.vscode'); do
  cd "$PATH" || exit
  TITLE=${PWD##*/}
  echo "  {"
  echo "    title: \"$TITLE\""
  echo "    paths: ["
  echo "      \"$PATH\""
  echo "    ]"
  echo "  }"
  cd - &>/dev/null || exit
done
echo "]"
