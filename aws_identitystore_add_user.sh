#!/usr/bin/env bash
set -euo pipefail


FIRSTNAME=$1
LASTNAME=$2
EMAIL=${3:-"${FIRSTNAME,,}.${LASTNAME,,}@quartr.se"}
# Root account credential
ID_STORE=${4:-"d-c3673fc59f"}
GROUP_ID=${5:-"201cb94c-2051-7062-e94b-cb8f093f160b"} # quartr-dev # quartr-ops b00c196c-20e1-7000-e058-1ee64f9150ef
ACCOUNT_ID=${6:-"256488951374"}
ACCOUNT_ROLE=${7:-AWSAdministratorAccess}

eval "$(aws-sso eval --account "$ACCOUNT_ID" --role "$ACCOUNT_ROLE")"

# Notes : 
# aws identitystore list-groups --identity-store-id $ID_STORE
# aws identitystore list-users --identity-store-id $ID_STORE
# aws identitystore list-users --identity-store-id $ID_STORE | jq -r '.Users[]|select(.UserName | contains("$EMAIL")).UserId'
# aws identitystore create-group --identity-store-id $ID_STORE --display-name "quartr-interns"

# Milan 
#   UID f04c196c-2051-709a-f8d6-b9ee69f02dfc
#   Ops group MID 205c199c-2051-7088-c7aa-79a5cdea7d25
# José 
#   UID f0acd9dc-4011-70e1-dd36-158fd33c0555
#   

USER_ID=$(aws identitystore create-user --identity-store-id "$ID_STORE" --user-name "$EMAIL" --emails "Value=$EMAIL,Primary=true" --name="FamilyName=$LASTNAME,GivenName=$FIRSTNAME" --display-name "$FIRSTNAME $LASTNAME" | jq -r '.UserId')

MEMBERSHIP_ID=$(aws identitystore create-group-membership --identity-store-id "$ID_STORE" --group-id "$GROUP_ID" --member-id "UserId=$USER_ID" | jq -r '.MembershipId')

echo "User '$FIRSTNAME $LASTNAME' created in store $ID_STORE, ID=$USER_ID, member of group $GROUP_ID, membership ID=$MEMBERSHIP_ID"
