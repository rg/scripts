#!/usr/bin/env bash

kv=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)
curl -Lo kubectl \
  https://storage.googleapis.com/kubernetes-release/release/$kv/bin/linux/amd64/kubectl &&
  chmod +x kubectl &&
  sudo mv kubectl /usr/local/bin/
