# Install Chocolatey
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

# Basic tooling
choco install -y firefox atom greenshot 7zip filezilla git bginfo kitty
# choco install -y python pip
# choco install -y slack
# choco install -y vagrant virtualbox fiddler4 curl wget wireshark nmap jq

# Atoms plugins
apm install atom-beautify file-icons linter pdf-view sort-lines todo-show highlight-selected minimap minimap-highlight-selected auto-update-packages
apm install linter-markdown language-markdown markdown-toc markdown-image-helper markdown-pdf markdown-scroll-sync # markdown-themeable-pdf
# apm install pretty-json rest-client Remote-FTP git-plus
# apm install pigments
# apm install autocomplete-python python-indent language-python
# apm install language-nginx
# apm install autocomplete-ansible language-ansible linter-ansible linter-ansible-linting
# apm install asciidoc-assistant asciidoc-image asciidoc-preview autocomplete-asciidoc language-asciidoc
