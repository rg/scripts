#!/usr/bin/env bash
set -euo pipefail

command -v certutil &>/dev/null || sudo apt install -yq libnss3-tools

VERSION=$(curl -s https://api.github.com/repos/FiloSottile/mkcert/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | tail -n1)
sudo curl --progress-bar -L "https://github.com/FiloSottile/mkcert/releases/download/$VERSION/mkcert-${VERSION}-linux-amd64" -o /usr/local/bin/mkcert
sudo chmod +x /usr/local/bin/mkcert
mkcert --version
