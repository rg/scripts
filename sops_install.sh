#!/usr/bin/env bash
set -euo pipefail

ORG="mozilla"
APP="sops"
URL="https://github.com/$(curl -s https://github.com/${ORG}/${APP}/releases | grep download | grep linux | grep amd64 | head -n1 | cut -d'"' -f2)"
sudo curl --progress-bar -L "${URL}" -o /usr/local/bin/$APP 
chmod +x "/usr/local/bin/$APP" 
"/usr/local/bin/$APP" --version
