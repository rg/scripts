#!/usr/bin/env bash

VERSION=$(curl -s 'https://api.github.com/repos/kubernetes/kompose/tags' | jq -r '.[].name' | sort --version-sort | tail -n1)

curl -L "https://github.com/kubernetes/kompose/releases/download/$VERSION/kompose-linux-amd64" -o kompose

chmod +x kompose
sudo mv ./kompose /usr/local/bin/kompose
kompose version
