#!/usr/bin/env bash

usage() {
  echo Replace a securigy group by another in a given AWS region
  echo
  echo aws_ec2_security_group_replace.sh [REGION] [OLD_SG] [NEW_SG]
  echo
  echo Example:
  echo aws_ec2_security_group_replace.sh eu-west-2 sg-09f7439acb4db4cdf sg-0b2bb3c6634cbad5
}

if [ $# -ne 3 ]; then
  usage
  exit 1
fi

REGION="$1"
OLD_SG="$2"
NEW_SG="$3"

INSTANCES=$(aws --region "$REGION" ec2 describe-instances | jq -r '.Reservations[].Instances[].InstanceId')

for i in $INSTANCES; do
  echo "Instance $i"
  SGS=$(aws --region "$REGION" ec2 describe-instance-attribute --instance-id "$i" --attribute groupSet | jq -r '.Groups[].GroupId' | tr '\n' ' ')
  echo "  Old security groups: $SGS"
  echo "  New security groups: ${SGS/$OLD_SG/$NEW_SG}"
  # shellcheck disable=SC2086
  aws --region "$REGION" ec2 modify-instance-attribute --instance-id "$i" --groups ${SGS/$OLD_SG/$NEW_SG}
done
