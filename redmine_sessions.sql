select distinct remote_host from (select c.remotehost as remote_host, 
		c.remoteport as remote_port,
		s.process as process_id,       
       s.user_name as username,
       s.starttime as session_start_time,
       s.db_name,
       i.starttime as current_query_time,
       i.text as query 
from STL_SESSIONS s
left join pg_user u on u.usename = s.user_name
left join stl_connection_log c
          on c.pid = s.process
          and c.event = 'authenticated'
left join stv_inflight i 
          on u.usesysid = i.userid
          and s.process = i.pid
where username <> 'rdsdb'
order by session_start_time desc)
where username = 'sync_aurora_to_redshift'
order by remote_host;
