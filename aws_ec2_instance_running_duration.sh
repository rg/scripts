#!/bin/bash

# Replace with your instance ID
instance_id="$1"

# Get the launch time and termination time for the instance
instance_info=$(aws ec2 describe-instances --instance-ids "$instance_id")

# Extract launch time and termination time from the instance info
launch_time=$(echo "$instance_info" | jq -r '.Reservations[0].Instances[0].LaunchTime')
termination_time=$(echo "$instance_info" | jq -r '.Reservations[0].Instances[0].StateTransitionReason' | grep -oP '(?<=terminated: )[^,]+')

# Convert ISO 8601 time format to Unix timestamp
launch_timestamp=$(date -d "$launch_time" +%s)
termination_timestamp=$(date -d "$termination_time" +%s)

# Calculate the runtime in seconds
runtime_seconds=$((termination_timestamp - launch_timestamp))

# Format the runtime in a human-readable way
runtime_formatted=$(date -u -d @$runtime_seconds +'%H:%M:%S')

echo "Instance $instance_id was running for $runtime_formatted"
