#!/usr/bin/env bash

curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3
. ~/.poetry/env
if [ ! -f $ZSH/plugins/poetry/_poetry ]; then
  mkdir -p $ZSH/plugins/poetry
  poetry completions zsh >$ZSH/plugins/poetry/_poetry
fi
