#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/bootandy/dust/git/refs/tags | jq -r '.[].ref' | grep v | cut -d'/' -f3 | tail -n1)
curl --progress-bar -LO "https://github.com/bootandy/dust/releases/download/$VERSION/dust-$VERSION-x86_64-unknown-linux-gnu.tar.gz"
tar -xf dust*.tar.gz --wildcards "*/dust" --strip-components 1
sudo mv dust /usr/local/bin/
rm dust*.tar.gz
