#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ipaddress import ip_network
import typer


def main(cidr: str, excludes: list[str]):
    result = [ip_network(cidr)]
    for x in excludes:
        n = ip_network(x)
        new = []
        for y in result:
            if y.overlaps(n):
                new.extend(y.address_exclude(n))
            else:
                new.append(y)
        result = new

    print(','.join(str(x) for x in sorted(result)))

if __name__ == "__main__":
    typer.run(main)

