#!/usr/bin/env bash

usage() {
  echo Remove all security group rules from a security grou, which allow its deletion
  echo
  echo $0 [REGION] [SG]
  echo
  echo Example:
  echo $0 eu-central-1 sg-09f7439acb4db4cdf
}

if [ $# -ne 2 ]; then
  usage
  exit 1
fi

REGION="$1"
SG="$2"

RULES=$(aws ec2 describe-security-group-rules | jq -r --arg sg $SG '.SecurityGroupRules[] | select(.GroupId=="$sg") | .SecurityGroupRuleId')

for RULE in $RULES; do
  echo "Rule $RULE"
done

echo Script not finished
exit 1
