#!/usr/bin/env bash

if [ $# -lt 1 ]; then
  echo "Display tags for a given ECR registry"
  echo
  echo "Usage: $0 registry [filter]"
  echo
  echo "  registry:     Name of the registry in the current AWS account and region"
  echo "  filter:       Optional regular expression to search for a specific tag"
  exit 1
fi

REGISTRY=${1}
FILTER=${2:-""}

aws ecr describe-images --repository-name $REGISTRY | jq -r '.imageDetails[].imageTags[0]' | grep -E -v 'null' | sort | grep -E "$FILTER"
