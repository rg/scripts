    #!/bin/bash

    cmd="ffmpeg"
    codec="libx264"
    extentions="avi AVI mp4 MP4 mov MOV mts MTS"
    target="mkv"

    # Arg test and usage message
    if [ $# -ne 1 ]
    then 
        echo "Usage: $0 top_directory"
        echo
        echo "Will find all $extentions video files under top_directory to encode them all into $target with $cmd using $codec"
        exit 1
    fi
    racine=$1

    # Test if cmd is available
    which $cmd &>/dev/null || bash 'echo "$cmd not available"; exit 1;'
    # Test if codec is available
    $cmd -encoders 2>/dev/null | grep -q $codec && echo "Encoding $extentions videos into $target with $cmd using $codec" || bash 'echo "$codec not available in $cmd"; exit 2;'

    shopt -s globstar
    for ext in $extentions
    do
        echo "Extention $ext"
        for file in **/*.$ext
        do
            if [[ -f "$file" ]]
            then
                echo -e "\t${file%.*}.$target"
                $cmd -i "$file" -c:v $codec -crf 28 -preset medium "${file%.*}.$target" &> videoEncoder.log
                if [ $? -ne 0 ]
                then
                    echo -e "\tError encoding $file, stopping"
                    exit 3
                else                 
                    mv "$file" "$file.done"
                    encoded="$encoded\n\t$file.done"
                fi
            fi
        done
    done

    echo "To be removed or whatever"
    echo -e $encoded
