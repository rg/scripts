#!/usr/bin/env bash

PROJECT=$1
REPOSITORY=$2
OS=${3:-linux}
ARCH=${4:-amd64}

curl -s https://api.github.com/repos/$PROJECT/$REPOSITORY/releases/latest | jq -r --arg R "$REPOSITORY" --arg O "$OS" --arg A "$ARCH" '.assets[]|select(.name=="$R-$O-$A")|.browser_download_url'

# Something doesn't work around JQ args :-$
