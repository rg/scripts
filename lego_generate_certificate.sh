#!/usr/bin/env bash

export LEGO_VERSION=4.6.0
export MAIL=remy.garrigue@gostudent.org
# export DOMAIN=nats-node-0.prod.product.frankfurt.gostudent.cloud
export DOMAIN=nats0.prod.product.aws.gostudent.cloud

# Install lego
if ! lego --version 2>/dev/null; then
    apt update -qq
    apt install -y -qq gzip tar curl
    cd /usr/bin || exit 1
    curl -Ls https://github.com/go-acme/lego/releases/download/v${LEGO_VERSION}/lego_v${LEGO_VERSION}_linux_amd64.tar.gz | gunzip -d | tar x
    lego --version
fi

# Generate certificate
lego --accept-tos --path /etc/letsencrypt --email $MAIL --domains $DOMAIN --http run
# Staging server for test
# lego --accept-tos --path /etc/letsencrypt --email $MAIL --domains $DOMAIN --http --server https://acme-staging-v02.api.letsencrypt.org/directory run

# Link files for NATS usage
{
    unlink node.cert.pem
    unlink node.key.pem
    unlink node.ca.pem
} &>/dev/null
ln -sf /etc/letsencrypt/certificates/$DOMAIN.crt /home/ubuntu/node.cert.pem
ln -sf /etc/letsencrypt/certificates/$DOMAIN.key /home/ubuntu/node.key.pem
ln -sf /etc/letsencrypt/certificates/$DOMAIN.issuer.crt /home/ubuntu/node.ca.pem

# Add crontab foor renewal
cat <<EOCRON >/etc/cron.daily/nats_certificate_renew
#!/usr/bin/env bash
lego --accept-tos --path /etc/letsencrypt --email $MAIL --domains $DOMAIN --http renew --renew-hook "nats-server --signal reload"
EOCRON
chmod +x /etc/cron.daily/nats_certificate_renew
