#!/usr/bin/env bash
set -xe

URL="https://github.com/$(curl -s https://github.com/gruntwork-io/kubergrunt/releases | grep download | grep linux_amd64 | head -n1 | cut -d'"' -f2)"
sudo curl -s -o /usr/local/bin/kubergrunt -L $URL
sudo chmod +x /usr/local/bin/kubergrunt
kubergrunt --version
