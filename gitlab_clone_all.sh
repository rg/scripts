#!/usr/bin/env bash

# v1, kept for API usage example
#namespace=${1:-"ansible"}
#gitlab -o json project list --all | jq -r ".[]|select(.namespace.name==$namespace)|.ssh_url_to_repo" | xargs -l git clone

pip3 install -un gitlabber
URL=$1
TOKEN=$2
gitlabber -t $TOKEN -u $URL -n path .
