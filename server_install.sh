#!/usr/bin/env bash

WKDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

sudo add-apt-repository -y universe
sudo apt-get update
sudo apt-get upgrade -y -qq

sudo apt-get install -y -qq \
  apt-file \
  apt-transport-https \
  build-dep \
  ca-certificates \
  coreutils \
  curl \
  dnsmasq-base \
  ebtables \
  etckeeper \
  ffmpeg \
  fontconfig \
  fonts-firacode \
  git \
  golang \
  imapfilter \
  jq \
  libvirt \
  libvirt-bin \
  libvirt-dev \
  libxml2-dev \
  libxslt-dev \
  mdp \
  mlocate \
  msttcorefonts \
  myrepos \
  neovim \
  nfs-kernel-server \
  nmap \
  qemu \
  ruby-dev \
  ruby-libvirt \
  screen \
  shellcheck \
  silversearcher-ag \
  snapd \
  software-properties-common \
  strace \
  tmux \
  ubuntu-restricted-extras \
  vagrant \
  vagrant-hostmanager \
  vagrant-libvirt \
  xclip \
  xfonts-utils \
  xsel \
  zlib1g-dev \
  zsh

sh "$WKDIR/bat_install.sh"
sh "$WKDIR/bandwhich_install.sh"
sh "$WKDIR/fd_install.sh"
if [ ! -f "$HOME/.gitconfig" ]; then
  cp "$WKDIR/gitconfig" "$HOME/.gitconfig"
fi

# GPG
cat <<EOGPG >~/.gnupg/gpg.conf
use-agent
pinentry-mode loopback
EOGPG
echo "allow-loopback-pinentry" >~/.gnupg/gpg-agent.conf
echo RELOADAGENT | gpg-connect-agent
gpg --generate-key

#
# Oh my zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
sudo sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
cp "$WKDIR/zshrc" "$HOME/.zshrc"
sudo cp "$WKDIR/zshrc" "$HOME/.zshrc"

# Python
sudo apt-get install -y python3 python3-pip tox
sudo pip3 install black click cookiecutter credstash crudini cryptography glances httpie requests shellcheck tox-run-command youtube-dl flake8 isort git-pull-request
sh "$WKDIR/pyenv_install.sh"
sh "$WKDIR/poetry_install.sh"

# Ansible
sudo pip3 install -U ansible 'molecule[docker]' molecule-vagrant

# AWS
pip3 install -U awscli
sh "$WKDIR/awless_install.sh"

# Docker
curl -fsSL https://get.docker.com | sudo sh
sudo pip3 install -U docker docker-compose
sudo usermod -a -G docker "$(whoami)"

# HashiCorp
sh "$WKDIR/packer_install.sh"
sh "$WKDIR/terraform_install.sh"
cp ./terraformrc ~/.terraformrc
sh "$WKDIR/terragrunt_install.sh"

# k8s
sh "$WKDIR/minikube_install.sh"
sh "$WKDIR/minikube_configure_kvm_driver.sh"
sh "$WKDIR/kubectl_install.sh"
sh "$WKDIR/helm_install.sh"

# Openstack
pip3 install python-openstackclient

# (Space)Vim
curl -sLf https://spacevim.org/install.sh | sudo bash
mkdir -p "$HOME/.SpaceVim.d/autoload"
cp "$WKDIR/spacevim_init.toml" "$HOME/.SpaceVim.d/init.toml"
cp "$WKDIR/spacevim_myspacevim.vim" "$HOME/.SpaceVim.d/autoload/myspacevim.vim"
sudo mkdir -p "$HOME/.SpaceVim.d/autoload"
sudo cp "$WKDIR/spacevim_init.toml" "$HOME/.SpaceVim.d/init.toml"
sudo cp "$WKDIR/spacevim_myspacevim.vim" "$HOME/.SpaceVim.d/autoload/myspacevim.vim"

# Cleaning
sudo apt-get autoremove -y

# Indexing
sudo apt-file update
sudo updatedb
