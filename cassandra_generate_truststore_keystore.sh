set -e

# There should be files there
ls /opt/scylla/tls/
# ca.key.pem  ca.pem  ca.srl  scylla.cnf  scylla.crt  scylla.csr  scylla.key.pem

# No trust, check those
openssl x509 -noout -modulus -in /opt/scylla/tls/ca.pem | openssl md5
openssl rsa -noout -modulus -in /opt/scylla/tls/ca.key.pem | openssl md5
openssl x509 -noout -modulus -in /opt/scylla/tls/scylla.crt | openssl md5
openssl rsa -noout -modulus -in /opt/scylla/tls/scylla.key.pem | openssl md5
openssl verify -CAfile /opt/scylla/tls/ca.pem /opt/scylla/tls/scylla.crt

# RC should exist
cat /root/.cassandra/cqlshrc
# [authentication]
# username = cassandra
# password = REDACTED

# [connection]
# hostname = 10.2.58.17 # ss -tlnp | grep ':9142' | awk '{print $4}' | cut -d':' -f1
# port = 9142
# ssl = true

# [ssl]
# version = TLSv1_2
# certfile = /opt/scylla/tls/ca.pem
# validate = true
# userkey = /opt/scylla/tls/scylla.key.pem
# usercert = /opt/scylla/tls/scylla.crt

# Hence Cassandra shell should works
cqlsh --ssl -e "show version"

export KS_FILE=/opt/scylla/tls/keystore.jks
export TS_FILE=/opt/scylla/tls/truststore.jks

rm -f $KS_FILE $TS_FILE

echo

# All good, now generate the Java keystore for the node certs
P12_PASS=$(uuidgen | md5sum | awk '{print $1}')
export KS_PASS=$(uuidgen | md5sum | awk '{print $1}')
openssl pkcs12 -export -out keystore.p12 -inkey /opt/scylla/tls/scylla.key.pem -in /opt/scylla/tls/scylla.crt -passout "pass:$P12_PASS"
keytool -importkeystore \
  -srcstoretype PKCS12 -srckeystore keystore.p12 -srcstorepass "$P12_PASS" \
  -destkeystore $KS_FILE -deststorepass "$KS_PASS" -destkeypass "$KS_PASS"
rm keystore.p12

echo

# And generate the Java truststore for the trust provider
P12_PASS=$(uuidgen | md5sum | awk '{print $1}')
export TS_PASS=$(uuidgen | md5sum | awk '{print $1}')

openssl pkcs12 -export -out truststore.p12 -inkey /opt/scylla/tls/ca.key.pem -in /opt/scylla/tls/ca.pem -passout pass:$P12_PASS
keytool -importkeystore \
	-srcstoretype PKCS12 -srckeystore truststore.p12 -srcstorepass $P12_PASS \
	-destkeystore $TS_FILE -deststorepass "$TS_PASS" -destkeypass "$TS_PASS"
rm truststore.p12

echo

cassandra-stress version

export NODE_IP=$(ss -tlnp | grep ':9142' | awk '{print $4}' | cut -d':' -f1)
echo
echo "Test. We don't really care about the result as long as 'Connected to cluster' displays"
timeout 5s cassandra-stress counter_write -node ${NODE_IP} -port native=9142 -transport keystore=$KS_FILE keystore-password=$KS_PASS truststore=$TS_FILE truststore-password=$TS_PASS -mode native cql3 user=$(grep username /root/.cassandra/cqlshrc | cut -d' ' -f3) password=$(grep password /root/.cassandra/cqlshrc | cut -d' ' -f3) 2>&1 | grep 'Connected to cluster'

echo

# Don't forget the passwords
echo "$KS_FILE pass: $KS_PASS"
echo "$TS_FILE pass: $TS_PASS"
