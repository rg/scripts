#!/usr/bin/env bash
set -xe

URL="https://github.com/$(curl -s https://github.com/go-acme/lego/releases | grep download | grep linux_amd64 | head -n1 | cut -d'"' -f2)"
curl -sL $URL | sudo tar -xz -C /usr/local/bin lego
lego --version
