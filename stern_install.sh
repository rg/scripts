#!/usr/bin/env bash
set -euo pipefail

ORG="stern"
APP="stern"
URL="https://github.com/$(curl --progress-bar https://github.com/${ORG}/${APP}/releases | grep download | grep linux_amd64 | head -n1 | cut -d'"' -f2)"
curl -sL "${URL}" | sudo tar -xz -C /usr/local/bin

"/usr/local/bin/$APP" --version
