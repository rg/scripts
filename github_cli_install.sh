#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/cli/cli/git/refs/tags | jq -r '.[].ref' | grep -v rc | cut -d'/' -f3 | sort --version-sort | tail -n1 | sed 's;v;;')
curl -LO "https://github.com/cli/cli/releases/download/v${VERSION}/gh_${VERSION}_linux_amd64.tar.gz"
tar -xf "gh_${VERSION}_linux_amd64.tar.gz"
sudo mv "gh_${VERSION}_linux_amd64/bin/gh" /usr/local/bin/
rm -rf gh_${VERSION}_linux_amd64*
