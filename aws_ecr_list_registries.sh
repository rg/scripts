#!/usr/bin/env bash

aws ecr describe-repositories | jq -r '.repositories[].repositoryName' | sort
