#!/usr/bin/env bash

# Requirements to build python
# Taken from the wiki, with s/python-/python3-
sudo apt-get install -y build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python3-openssl git

# PyEnv install
curl -s https://pyenv.run | bash
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

LATEST37=$(pyenv install --list | grep ' 3.7' | grep -v dev | grep -v a | tail -n1 | sed 's;\s*;;g')
LATEST38=$(pyenv install --list | grep ' 3.8' | grep -v dev | grep -v a | tail -n1 | sed 's;\s*;;g')
LATEST39=$(pyenv install --list | grep ' 3.9' | grep -v dev | grep -v a | tail -n1 | sed 's;\s*;;g')
LATEST310=$(pyenv install --list | grep ' 3.10' | grep -v dev | grep -v a | tail -n1 | sed 's;\s*;;g')
pyenv install "$LATEST37"
pyenv install "$LATEST38"
pyenv install "$LATEST39"
pyenv install "$LATEST310"
pyenv global "$LATEST37" "$LATEST38" "$LATEST39" "$LATEST310"
