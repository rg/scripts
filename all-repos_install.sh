#!/usr/bin/env bash

pip3 install all-repos
if [ ! -f ~/.all-repos.json ]; then
  cp all-repos.json ~/.all-repos.json
fi
chmod 0600 ~/.all-repos.json
all-repos-clone -C ~/.all-repos.json
