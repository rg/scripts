#!/usr/bin/env python3

import boto3
import click


@click.group()
def cli():
    pass


@cli.group()
def zone():
    pass


@zone.command("list")
@click.option("-o", "--output", default="Name", type=click.Choice(["Id", "Name"]))
def zone_list(output):
    response = client.list_hosted_zones_by_name()
    for result in [zone[output] for zone in response["HostedZones"]]:
        print(result)


@zone.command("show")
@click.option("-i", "--id", help="Zone Id")
def zone_show(id):
    response = client.get_hosted_zone(Id=id)
    if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
        print("Name:\t" + response["HostedZone"]["Name"])
        print("Id:\t" + response["HostedZone"]["Id"])
        for dns in response["DelegationSet"]["NameServers"]:
            print("DNS:\t" + dns)
    else:
        print(response)


@cli.group()
def record():
    pass


@record.command("list")
@click.option("-z", "--zone", help="Zone Id")
def record_list(zone):
    response = client.get_hosted_zone(Id=zone)
    print(response)


client = boto3.client("route53")


if __name__ == "__main__":
    cli()
