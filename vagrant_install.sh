#!/usr/bin/env bash
set -e

curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

sudo apt-get build-dep vagrant ruby-libvirt
sudo apt-get install -y libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev
vagrant plugin install vagrant-libvirt
vagrant plugin install vagrant-hostmanager

sudo cp vagrant_sudoers.conf /etc/sudoers.d/80-vagrant.conf
