#!/usr/bin/env bash
set -xe

# Install (or update to) the latest version of terraform in /usr/local/bin

LATEST_VERSION=$(curl --silent "https://api.github.com/repos/hashicorp/terraform/releases/latest" | jq -r .tag_name)
VERSION=${1:-$LATEST_VERSION}

curl -s -o terraform.zip https://releases.hashicorp.com/terraform/${VERSION/v/}/terraform_${VERSION/v/}_linux_amd64.zip
unzip terraform.zip
rm terraform.zip
chmod +x terraform
sudo mv terraform /usr/local/bin/
terraform version
