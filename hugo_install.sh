#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/gohugoio/hugo/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | tail -n1)
curl --progress-bar -LO "https://github.com/gohugoio/hugo/releases/download/$VERSION/hugo_extended_${VERSION/v/}_Linux-64bit.tar.gz"
tar -xf hugo*.tar.gz hugo
sudo mv hugo /usr/local/bin/
rm hugo*.tar.gz
