#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/nvm-sh/nvm/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/$VERSION/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

nvm install --lts --latest-npm
npm install -g yarn
