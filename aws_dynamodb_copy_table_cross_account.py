#!/usr/bin/env python
# -*- coding: utf-8 -*-

import boto3
import os
import typer
from rich.pretty import pprint
from rich.progress import track


def main(
    source_access_key_id: str,
    source_secret_access_key: str,
    source_session_token: str,
    source_table: str,
    destination_access_key_id: str,
    destination_secret_access_key: str,
    destination_session_token: str,
    destination_table: str,
):
    source = boto3.session.Session(
        aws_access_key_id=source_access_key_id,
        aws_secret_access_key=source_secret_access_key,
        aws_session_token=source_session_token,
    ).client("dynamodb")
    destination = boto3.session.Session(
        aws_access_key_id=destination_access_key_id,
        aws_secret_access_key=destination_secret_access_key,
        aws_session_token=destination_session_token,
    ).client("dynamodb")

    paginator = source.get_paginator("scan")
    response = paginator.paginate(
        TableName=source_table,
        Select="ALL_ATTRIBUTES",
        ReturnConsumedCapacity="NONE",
        ConsistentRead=True,
    )
    for page in track(response):
        for item in page["Items"]:
            try:
                destination.put_item(TableName=destination_table, Item=item)
            except Exception as e:
                pprint(e)


if __name__ == "__main__":
    typer.run(main)
