#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Requires boto3 
#     apk add py3-boto3
# List the queues & try to send & receive
#     aws sqs list-queues | jq -r '.QueueUrls[]'
#     python3 test_sqs.py send --queue-url https://eu-north-1.queue.amazonaws.com/651142232517/quartr-development-data-create-low-prio.fifo --message-group-id test --message test 
#     python3 test_sqs.py receive --queue-url https://eu-north-1.queue.amazonaws.com/651142232517/quartr-development-data-create-low-prio.fifo

import argparse
import boto3
import time

# Initialize SQS client
sqs = boto3.client('sqs', region_name='eu-north-1')

def send_message(queue_url, message_body, message_group_id=None):
    try:
        # Send message to SQS queue
        if message_group_id:
            response = sqs.send_message(
                QueueUrl=queue_url,
                MessageBody=message_body,
                MessageGroupId=message_group_id
            )
        else:
            response = sqs.send_message(
                QueueUrl=queue_url,
                MessageBody=message_body
            )
        print("Message sent successfully. Message ID:", response['MessageId'])
    except Exception as e:
        print("Error:", e)

def receive_messages(queue_url):
    try:
        # Receive messages from SQS queue
        response = sqs.receive_message(
            QueueUrl=queue_url,
            MaxNumberOfMessages=1,
            WaitTimeSeconds=20  # Long polling to reduce the number of empty responses
        )

        # Check if there are any messages
        if 'Messages' in response:
            for message in response['Messages']:
                # Process the message
                print("Received message:", message['Body'])
                # Delete the message from the queue
                sqs.delete_message(
                    QueueUrl=queue_url,
                    ReceiptHandle=message['ReceiptHandle']
                )
        else:
            print("No messages available")
    except Exception as e:
        print("Error:", e)

def main():
    parser = argparse.ArgumentParser(description="Send or receive messages from an SQS queue.")
    subparsers = parser.add_subparsers(dest="command")

    send_parser = subparsers.add_parser("send", help="Send a message to the SQS queue")
    send_parser.add_argument("--queue-url", required=True, help="URL of the SQS queue")
    send_parser.add_argument("--message", required=True, help="Message to be sent")
    send_parser.add_argument("--message-group-id", help="Message group ID for FIFO queues")

    receive_parser = subparsers.add_parser("receive", help="Receive messages from the SQS queue")
    receive_parser.add_argument("--queue-url", required=True, help="URL of the SQS queue")

    args = parser.parse_args()

    if args.command == "send":
        send_message(args.queue_url, args.message, args.message_group_id)
    elif args.command == "receive":
        receive_messages(args.queue_url)

if __name__ == "__main__":
    main()
