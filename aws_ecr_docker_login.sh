#!/usr/bin/env bash

REGION=$(aws configure list | awk '/region/ {print $2}')
REGISTRY=$(aws ecr describe-registry | jq -r '.registryId')

aws ecr get-login-password | docker login --username AWS --password-stdin $REGISTRY.dkr.ecr.$REGION.amazonaws.com 2>&1 | grep -v -E '^$|WARNING|credential'
