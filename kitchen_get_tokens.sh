#!/usr/bin/env bash

for arg in $*; do
  ag --nonumbers --nofile -G kitchen.yml formulas-$arg ~/sources/salt -B2 -A1 | tail -n4
done
