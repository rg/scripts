#!/usr/bin/env bash

if [ $# -ne 1 ]; then
  echo "Usage: $0 couchdb_ip_or_host or $0 login:pass@couchdb_ip_or_host"
  exit 1
fi

for db in _users _metadata _replicator _global_changes _config; do
  curl -sSkL -X PUT "http://$1:5984/${db}"
done
