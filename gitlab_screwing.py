#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Just using Gitlab's Python SDK when you're too lazy to click

https://python-gitlab.readthedocs.io/en/stable/api-usage.html
"""

import gitlab
import click
import urllib3

urllib3.disable_warnings()


gl = None


@click.group()
@click.option("-u", "--url", default="https://gitlab.local")
@click.option("-t", "--token", default="")
def cli(url, token):
    """CLI, aka command line interface entrypoint / top function"""
    global gl
    gl = gitlab.Gitlab(url, token, api_version=4, ssl_verify=False)


@click.command()
@click.option("-f", "--filter", default=None)
def project(filter=None):
    """CLI nested command for projects related stuff"""
    global gl
    projects = gl.projects.list(search=filter, all=True)
    for p in projects:
        print(p.name)


if __name__ == "__main__":
    cli()
