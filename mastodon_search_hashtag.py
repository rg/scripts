#!/usr/bin/env python
# -*- coding: utf-8 -*-

from mastodon import Mastodon
import typer

app = typer.Typer()

@app.command()
def search(
    access_token: str = typer.Option(..., help="Mastodon access token"),
    hashtag: str = typer.Option(..., help="Hashtag to search for"),
    keywords: str = typer.Option(..., help="Keywords to filter search results by"),
):
    # Set up a Mastodon instance with the provided access token
    mastodon = Mastodon(
        access_token=access_token,
        api_base_url="https://mastodon.social",
    )

    # Split the keywords argument into a list of keywords
    keywords = keywords.split(",")

    # Search for the hashtag and filter for posts containing the keywords
    search_results = mastodon.search(hashtag)
    filtered_results = [status for status in search_results if any(keyword in status.content for keyword in keywords)]

    # Print out the filtered results
    for status in filtered_results:
        print(status.content)

if __name__ == "__main__":
    app()
