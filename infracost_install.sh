#!/usr/bin/env bash
set -euo pipefail

curl -fsSL https://raw.githubusercontent.com/infracost/infracost/master/scripts/install.sh | sh
