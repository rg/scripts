#!/usr/bin/env python
# -*- coding: utf-8 -*-

# https://github.com/flameshot-org/flameshot/issues/2868#issuecomment-1384310540

import dbus
bus = dbus.SessionBus()
perm = bus.get_object('org.freedesktop.impl.portal.PermissionStore',
                      '/org/freedesktop/impl/portal/PermissionStore')
perm_iface = dbus.Interface(
    perm, dbus_interface='org.freedesktop.impl.portal.PermissionStore')

perm_iface.Lookup("screenshot", "screenshot")
perm_iface.Set("screenshot", dbus.Boolean(True),
               "screenshot", {"": ["yes"]}, dbus.Byte(0x00))
perm_iface.Lookup("screenshot", "screenshot")
