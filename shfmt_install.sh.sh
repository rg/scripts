#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/mvdan/sh/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | grep -v dev | tail -n1)
curl --progress-bar -L "https://github.com/mvdan/sh/releases/download/$VERSION/shfmt_${VERSION}_linux_amd64" -o shfmt
chmod +x shfmt
sudo mv shfmt /usr/local/bin/
shfmt -version
