#!/usr/bin/env bash
# Install https://github.com/dandavison/delta
set -e

VERSION=$(curl -s https://api.github.com/repos/dandavison/delta/git/refs/tags | jq -r '.[].ref' | grep -v '\-' | cut -d'/' -f3 | sort --version-sort | tail -n1)
curl -LO "https://github.com/dandavison/delta/releases/download/$VERSION/delta-$VERSION-x86_64-unknown-linux-musl.tar.gz"
tar -xf delta*.tar.gz
sudo mv "delta-$VERSION-x86_64-unknown-linux-musl/delta" /usr/local/bin/
rm -rf delta*
