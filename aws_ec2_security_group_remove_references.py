import boto3
import typer
from botocore.exceptions import ClientError

app = typer.Typer()

@app.command()
def remove_security_group(
    security_group_id: str,
    dry_run: bool = typer.Option(False, "-d", "--dry-run", help="Perform a dry run without making changes.")
):
    """Remove the specified security group from all other security groups using it in ingress rules."""
    
    ec2 = boto3.client('ec2')

    try:
        # Get all security groups
        response = ec2.describe_security_groups()
        security_groups = response['SecurityGroups']

        for sg in security_groups:
            sg_id = sg['GroupId']
            ip_permissions = sg.get('IpPermissions', [])

            # Check if the security group is referenced in the ingress rules
            for permission in ip_permissions:
                for user_id_group_pair in permission.get('UserIdGroupPairs', []):
                    if user_id_group_pair['GroupId'] == security_group_id:
                        print(f"Found reference to {security_group_id} in ingress rules of {sg_id}")

                        if not dry_run:
                            # Remove the security group from the ingress rule
                            new_user_id_group_pairs = [
                                pair for pair in permission['UserIdGroupPairs']
                                if pair['GroupId'] != security_group_id
                            ]

                            # Revoke the old rule
                            ec2.revoke_security_group_ingress(GroupId=sg_id, IpPermissions=[permission])

                            # Add the new rule without the security group
                            if new_user_id_group_pairs:
                                new_permission = permission.copy()
                                new_permission['UserIdGroupPairs'] = new_user_id_group_pairs
                                ec2.authorize_security_group_ingress(GroupId=sg_id, IpPermissions=[new_permission])
                                print(f"Updated ingress rules for {sg_id} to remove {security_group_id}")
                        else:
                            print(f"[DRY RUN] Would remove {security_group_id} from ingress rules of {sg_id}")

    except ClientError as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    app()
