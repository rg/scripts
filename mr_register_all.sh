#!/usr/bin/env bash

# mr register all git folder
rm ~/.mrconfig

for d in $(find $HOME -name .git 2>/dev/null | sed 's;/.git;;' | egrep -v '\.terraform|\.cache|\.terragrunt-cache|\.bundle|\.pyenv|src|\.local|\.vscode|/themes/|.cargo|.oh-my-zsh' | sort); do
  echo $d
  mr register $d 1>/dev/null
done
