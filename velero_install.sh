#!/usr/bin/env bash
set -euo pipefail

VERSION="1.11.1"
curl -sL "https://github.com/vmware-tanzu/velero/archive/refs/tags/v$VERSION.tar.gz" | sudo tar -xz -C /usr/local/bin

"/usr/local/bin/$APP" --version
