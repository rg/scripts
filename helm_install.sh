#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/helm/helm/git/refs/tags | jq -r '.[].ref' | grep -v rc | cut -d'/' -f3 | sort --version-sort | tail -n1)
curl -LO https://get.helm.sh/helm-$VERSION-linux-amd64.tar.gz
tar -xf helm*.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/
rm -rf helm*.tar.gz linux-amd64
