#!/usr/bin/env bash

sudo apt-get install -y kodi

cat <<EOF >~/.kodi/userdata/keymaps/subtitles.xml
<keymap>
  <global>
    <keyboard>
      <left mod="ctrl">subtitledelayminus</left>
      <right mod="ctrl">subtitledelayplus</right>
    </keyboard>
  </global>
</keymap>
EOF
