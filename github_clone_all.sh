#!/bin/bash
# Quick and dirty script to checkout all my Github repositories at once

GITHUB_USER=${1:-"rgarrigue"}
WKDIR=$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)/roles

mkdir -p "$WKDIR"
for repo in $(curl -s "https://api.github.com/users/$GITHUB_USER/repos" | jq '.[] | select(.name | .ssh_url' | tr -d '"'); do
  role=${repo/.git//}
  if [ -d "$WKDIR/$role" ]; then
    cd "$WKDIR/$role" || exit 1
    echo "Updating $role"
    git pull -q
    cd - &>/dev/null || exit 2
  else
    echo "Cloning $repo in $role"
    git clone -q "$repo" "$WKDIR/$role"
  fi
done
