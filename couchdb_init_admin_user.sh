#!/usr/bin/env bash

if [ $# -ne 3 ]; then
  echo "Usage: $0 couchdb_ip_or_host login password"
fi

curl -X PUT "$1:5984/_node/_local/_config/admins/$2" -d "\"$3\""
