#!/usr/bin/env bash

pipx install elia-chat

curl -fsSL https://ollama.com/install.sh | sh
ollama pull llama3

cat <<EOCFG > $HOME/.config/elia/config.toml
[[models]]
name = "ollama/llama3"
EOCFG
