#!/usr/bin/env bash
set -euo pipefail

ORG="ogham"
APP="dog"
URL="https://github.com/$(curl -s https://github.com/${ORG}/${APP}/releases | grep download | grep x86_64-unknown-linux-gnu.zip | head -n1 | cut -d'"' -f2)"
curl -sL "${URL}" -o dog.zip
unzip dog.zip
sudo mv bin/dog /usr/local/bin
rm -rf dog.zip bin man completions

"/usr/local/bin/$APP" --version
