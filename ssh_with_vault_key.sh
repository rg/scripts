#!/usr/bin/env bash
set -e

SERVER_IP=${1}
if [ $# -ne 1 ]; then
  echo "Usage: $0 server_ip"
  exit 1
fi

export VAULT_ADDR=https://vaults.gostudent.org

if ! vault status &>/dev/null; then
    vault login -tls-skip-verify -method oidc
fi

vault write -tls-skip-verify -field signed_key ssh/sign/user public_key=@$HOME/.ssh/id_ed25519.pub  > ~/.ssh/vault
chmod 0600 ~/.ssh/vault

ssh -i ~/.ssh/vault "root@${SERVER_IP}"

