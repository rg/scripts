#!/usr/bin/env bash
set -euo pipefail

ORG="mikefarah"
APP="yq"
URL="https://github.com/$(curl -s https://github.com/${ORG}/${APP}/releases | grep download | grep yq_linux_amd64 | head -n1 | cut -d'"' -f2)"
sudo curl -sL "${URL}" -o /usr/local/bin/$APP
sudo chmod +x "/usr/local/bin/$APP"

"/usr/local/bin/$APP" --version
