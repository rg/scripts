#!/usr/bin/env bash
set -euo pipefail

go install github.com/davecheney/httpstat@latest
sudo mv $HOME/go/bin/httpstat /usr/local/bin/
