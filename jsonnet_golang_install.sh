#!/usr/bin/env bash
set -euo pipefail

ORG="google"
APP="go-jsonnet"
URL="https://github.com/$(curl -s https://github.com/${ORG}/${APP}/releases | grep download | grep Linux_x86_64.tar.gz | head -n1 | cut -d'"' -f2)"
curl -sL "${URL}" | sudo tar -xz -C /usr/local/bin

"/usr/local/bin/${APP/go-/}" --version
