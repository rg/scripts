#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/sharkdp/bat/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | tail -n1)
curl -LO "https://github.com/sharkdp/bat/releases/download/$VERSION/bat-$VERSION-x86_64-unknown-linux-gnu.tar.gz"
tar -xf bat*.tar.gz
sudo mv "bat-$VERSION-x86_64-unknown-linux-gnu/bat" /usr/local/bin/
rm -rf bat-*-gnu*
