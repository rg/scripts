#!/usr/bin/env bash

if [ ! -d .git ]; then
  git init .
fi

if [ ! -f .pre-commit-config.yaml ]; then

  # Basic hooks
  cat <<EOF >.pre-commit-config.yaml
---
# See https://pre-commit.com for more information
# See https://pre-commit.com/hooks.html for more hooks
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v3.2.0
    hooks:
      - id: trailing-whitespace
      - id: end-of-file-fixer
      - id: check-json
      - id: check-yaml
      - id: check-added-large-files
EOF

  # Terraform hooks
  if [ "$1" == "terraform" ]; then
    VERSION=$(curl -s https://api.github.com/repos/antonbabenko/pre-commit-terraform/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)
    cat <<EOF >>.pre-commit-config.yaml
  - repo: git://github.com/antonbabenko/pre-commit-terraform
    rev: $VERSION
    hooks:
      - id: terraform_fmt
EOF
  fi

fi # ! -f .pre-commit-config.yaml

pre-commit install
pre-commit run --all-files
