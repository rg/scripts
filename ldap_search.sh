#!/bin/bash
#
# Shortcut for LDAP search

ldapsearch -LLL -x -D "uid=remy,ou=people,dc=fr,dc=cozycloud,dc=cc" -b "dc=fr,dc=cozycloud,dc=cc" -H ldaps://ldap.cozycloud.cc -w "$(pass show remy/cozy/ldap)" $*
