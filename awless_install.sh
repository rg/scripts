#!/usr/bin/env bash

# VERSION=$(curl -s https://api.github.com/repos/wallix/awless/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)
VERSION=v0.1.11
curl -LO "https://github.com/wallix/awless/releases/download/$VERSION/awless-linux-amd64.tar.gz"
tar xf awless-linux-amd64.tar.gz
sudo mv awless /usr/local/bin/
rm -rf awless*.tar.gz
awless --version
