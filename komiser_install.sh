#!/usr/bin/env bash
set -xe

VERSION=$(curl -s 'https://api.github.com/repos/mlabouardy/komiser/tags' | jq -r '.[].name' | sort --version-sort | tail -n1)
wget "https://cli.komiser.io/$VERSION/linux/komiser"
chmod +x komiser
sudo mv komiser /usr/local/bin
komiser --version
