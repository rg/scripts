#!/usr/bin/env bash

ENVIRONMENT=${1:-dev}
aws sts get-caller-identity &>/dev/null || exit 1

IPS=$(
  { 
    aws elbv2 describe-load-balancers | jq -r '.LoadBalancers[].DNSName'
    aws elb describe-load-balancers | jq -r '.LoadBalancerDescriptions[].DNSName' 
  } | xargs dig +short | sed 's;$;/32;' | sort -u | tr '\n' ',' 
  aws ec2 describe-vpcs | jq -r '.Vpcs[0].CidrBlockAssociationSet[0].CidrBlock'
)
sudo sed -i "s;^AllowedIPs = .*;AllowedIPs = $IPS;" /etc/wireguard/wg$ENVIRONMENT.conf
sudo systemctl restart wg-quick@wg$ENVIRONMENT
