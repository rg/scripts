#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/ogham/exa/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | tail -n1)
curl --progress-bar -LO "https://github.com/ogham/exa/releases/download/$VERSION/exa-linux-x86_64-$VERSION.zip"
unzip -j exa*.zip bin/exa
sudo mv exa /usr/local/bin/
rm exa*.zip
