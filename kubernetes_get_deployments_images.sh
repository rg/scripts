#!/usr/bin/env bash

{
  kubectl get deployment -n backend -o jsonpath='{$..image}'
  kubectl get deployment -n frontend -o jsonpath='{$..image}'
} | tr ' ' '\n' | cut -d '/' -f3 | sort -u | tr ':' ';' | sed 's;gitlab.*$;;' | tee images.csv
