#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Usage
#    python aws_step_function_test.py --state-machine-arn arn:aws:states:eu-north-1:651142232517:stateMachine:quartr-development-data-sqs-delay --input-data '{"waitTimestamp":"2024-03-27T10:12:41+00:00","queueUrl":"https://sqs.eu-north-1.amazonaws.com/651142232517/quartr-development-data-update-low-prio.fifo","messageBody":"This is a test"}'

import argparse
import boto3
import time

# Initialize Step Functions client
sfn = boto3.client('stepfunctions', region_name='eu-north-1')

def start_execution(state_machine_arn, input_data):
    try:
        # Start execution of the Step Function
        response = sfn.start_execution(
            stateMachineArn=state_machine_arn,
            input=input_data
        )
        return response['executionArn']
    except Exception as e:
        print("Error starting execution:", e)
        return None

def describe_execution(execution_arn):
    try:
        # Describe execution to get its status
        response = sfn.describe_execution(
            executionArn=execution_arn
        )
        return response['status']
    except Exception as e:
        print("Error describing execution:", e)
        return None

def main():
    parser = argparse.ArgumentParser(description="Test an AWS Step Function")
    parser.add_argument("--state-machine-arn", required=True, help="ARN of the Step Function")
    parser.add_argument("--input-data", required=True, help="Input data for the Step Function (in JSON format)")

    args = parser.parse_args()

    # Start execution
    execution_arn = start_execution(args.state_machine_arn, args.input_data)
    if execution_arn is None:
        return

    print("Execution started. ARN:", execution_arn)

    # Wait for execution to complete
    while True:
        status = describe_execution(execution_arn)
        if status in ['SUCCEEDED', 'FAILED', 'TIMED_OUT', 'ABORTED']:
            print("Execution completed with status:", status)
            if status == 'FAILED':
                try:
                    response = sfn.get_execution_history(
                        executionArn=execution_arn,
                        reverseOrder=True
                    )
                    events = response['events']
                    for event in events:
                        if event['type'] == 'ExecutionFailed':
                            print("Error message:", event['executionFailedEventDetails']['cause'])
                            break
                except Exception as e:
                    print("Error getting failure details:", e)
            break
        else:
            print("Execution status:", status)
            time.sleep(5)  # Wait for 5 seconds before checking again

if __name__ == "__main__":
    main()
