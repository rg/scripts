#!/usr/bin/env bash

REGION=${1:-eu-central-1}
curl -s https://ip-ranges.amazonaws.com/ip-ranges.json | jq -r ".prefixes[] | select(.region==\"$REGION\") | select(.service==\"EC2_INSTANCE_CONNECT\") | .ip_prefix"
