#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/rebuy-de/aws-nuke/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)
curl -LO https://github.com/rebuy-de/aws-nuke/releases/download/$VERSION/aws-nuke-$VERSION-linux-amd64.tar.gz
tar -xf aws-nuke*.tar.gz
rm -rf aws-nuke*.tar.gz
sudo mv aws-nuke*-linux-amd64 /usr/local/bin/aws-nuke
aws-nuke --version
