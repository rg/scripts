#!/usr/bin/env bash

if [ $# -ne 1 ]; then
  echo "Usage: $0 container_name"
  exit 1
fi

docker inspect "$1" | jq '.[].NetworkSettings.Networks.bridge.IPAddress' | tr -d '"'
