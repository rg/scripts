#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This script was written for a Jenkins to download a war from a Nexus Repository Manager and deploy/undeploy it on a Wildfly.

TODO
    Rewrites comments from french to english
    Extend available actions
    Add a tqdm progress bar to wildfly.deploy()

Requirements
    yum install -y python-pip || apt install -y python-pip
    pip install -U pip argparse requests tqdm
"""

import argparse
import logging
import requests
from requests.auth import HTTPDigestAuth, HTTPBasicAuth
from tqdm import tqdm
import sys
import os


class Wildfly:
    """
    Classe représentant un wildfly et les opérations associées. Dans la version initiale il s'agit surtout de tout ce qui est déployer ou virer des war
    """

    def __init__(self, url, login="wildflyadmin", password="@adminWILDFLY"):
        self.url = url
        self.auth = HTTPDigestAuth(login, password)  # HTTPDigestAuth pour wildfly
        logging.debug("Création objet Wildfly " + str(self))
        self.check_connection()

    def __str__(self):
        return self.url

    def check_connection(self):
        """
        Vérifie la bonne connexion au wildfly, notamment login & mdp
        """
        try:
            r = requests.get(self.url + "/management", auth=self.auth)
            r.raise_for_status()
            logging.debug("Connexion au wildfly OK")
            return True
        except Exception as e:
            logging.error("Connexion au wildfly impossible: " + str(e))
            return False

    def upload(self, artifact):
        """
        Charge l'artefact sur le serveur wildfly, il atterira à priori dans le dossier standalone/tmp, sans pour autant être activé / déploié
        """
        logging.info("Chargement de " + str(artifact) + " sur " + str(self))
        try:
            url = self.url + "/management/add-content"
            files = {"file": open("/tmp/" + artifact.filename, "rb")}
            r = requests.post(url, auth=self.auth, files=files, stream=True)
            r.raise_for_status()
            artifact.bytes_value = r.json()["result"]["BYTES_VALUE"]
            return r.json()["outcome"] == "success"
        except Exception as e:
            logging.error("Impossible de charger l'artefact: " + str(e))
            return False

    def deploy(self, artifact):
        """
        Déploie un artefact qui doit avoir été chargé au préalable
        """
        logging.info("Déploiement de " + str(artifact) + " sur " + str(self))
        try:
            r = requests.post(
                self.url + "/management",
                auth=self.auth,
                headers={"Content-Type": "application/json"},
                json={
                    "operation": "add",
                    "enabled": "true",
                    "content": [{"hash": {"BYTES_VALUE": artifact.bytes_value}}],
                    "address": [{"deployment": str(artifact)}],
                },
            )
            r.raise_for_status()
            return r.json()["outcome"] == "success"
        except Exception as e:
            logging.error(
                "Impossible d'activer l'artefact: "
                + str(e)
                + ". Soit il est déjà déploié, soit le déploiement a échoué faute de dépendances ou de paramètres"
            )
            return False

    def undeploy(self, artifact):
        """
        Dé-déploie , i.e. désactive un artefact sur le wildfly
        """
        logging.info("Désactivation de " + str(artifact) + " sur " + str(self))
        try:
            r = requests.post(
                self.url + "/management",
                auth=self.auth,
                headers={"Content-Type": "application/json"},
                json={
                    "operation": "undeploy",
                    "address": [{"deployment": str(artifact)}],
                },
            )
            r.raise_for_status()
            return r.json()["outcome"] == "success"
        except Exception as e:
            logging.error("Impossible de désactiver l'artefact: " + str(e))
            return False

    def remove(self, artifact):
        """
        Supprime un artefact sur le serveur wildfly, il doit avoir été désactivé au préalable
        """
        logging.info("Suppression de " + str(artifact) + " sur " + str(self))
        try:
            r = requests.post(
                self.url + "/management",
                auth=self.auth,
                headers={"Content-Type": "application/json"},
                json={
                    "operation": "remove",
                    "address": [{"deployment": str(artifact)}],
                },
            )
            r.raise_for_status()
            return r.json()["outcome"] == "success"
        except Exception as e:
            logging.error("Impossible de supprimer l'artefact: " + str(e))
            return False

    def is_deployed(self, artifact):
        """
        Vérifie si l'artifact est déploié
        """
        logging.info(
            "Vérification du déploiement de " + str(artifact) + " sur " + str(self)
        )
        try:
            r = requests.get(self.url + "/management", auth=self.auth)
            r.raise_for_status()
            logging.debug(
                "Vérification si "
                + str(artifact)
                + " figure dans les déploiements "
                + str(r.json()["deployment"].keys())
            )
            result = str(artifact) in r.json()["deployment"]
            if result:
                logging.info("L'artifact est bien déploié")
            else:
                logging.info("L'artifact n'est pas déploié")
            return result

        except Exception as e:
            logging.error(
                "Impossible de vérifier le déploiement de l'artifact: " + str(e)
            )
            return False

    def list_deployed(self):
        """
        Retourne la list des artifacts déploiés
        """
        logging.info("Récupération de la liste des déploiements sur " + str(self))
        try:
            r = requests.get(self.url + "/management", auth=self.auth)
            r.raise_for_status()
            l = ""
            for a in r.json()["deployment"].keys():
                l += "- " + a + "\n"
            return l

        except Exception as e:
            logging.error(
                "Impossible de récupérer la liste des déploiements: " + str(e)
            )
            return False


class Artifact:
    """
    Cette classe représente un artefact au sens Nexus Repository Manager, par défaut un war, mais ça pourrait possiblement être un .ear, un .jar... non testé cela dit!
    """

    def __init__(self, **kwargs):
        """
        Nouvel artefact
        """
        # Soit via son URL
        if "url" in kwargs:
            self.url = kwargs["url"]
            if "login" in kwargs and "password" in kwargs:
                self.auth = HTTPBasicAuth(
                    kwargs["login"], kwargs["password"]
                )  # HTTPBasicAuth pour le Nexus
            self.filename = self.url.split("/")[-1]
        # Soit via filename
        elif "filename" in kwargs:
            self.filename = kwargs["filename"]
        else:
            raise ValueError("Mauvais arguments pour créer un artifact: " + str(kwargs))

        self.id = "-".join(self.filename.split("-")[:-1])
        self.version = ".".join(self.filename.split("-")[-1].split(".")[:-1])
        self.extension = self.filename.split(".")[-1]
        logging.debug("Création objet Artifact " + str(self))

    def __str__(self):
        return self.id + "-" + self.version + "." + self.extension

    def download(self):
        """
        Télécharge un artefact depuis son URL et le stocke en local dans /tmp/ (avant upload sur un wildfly)
        """
        try:
            # https://stackoverflow.com/questions/610883/how-to-know-if-an-object-has-an-attribute-in-python/610923#610923
            logging.info("Téléchargement de " + self.url)
            try:
                r = requests.get(self.url, auth=self.auth, stream=True)
            except AttributeError:
                r = requests.get(self.url, stream=True)

            total_size = int(r.headers.get("content-length", 0))
            chunk = 1024
            path = "/tmp/" + self.filename
            with open(path, "wb") as f:
                for data in tqdm(
                    r.iter_content(chunk_size=chunk),
                    total=int(total_size / chunk),
                    unit="KB",
                    unit_scale=False,
                    desc=path,
                ):
                    f.write(data)
                f.close()

            # OK or NOK
            r.raise_for_status()
            return True

        except Exception as e:
            logging.critical("Téléchargement échoué: " + str(e))
            return False

    def remove(self):
        """
        Supprime le fichier local
        """
        try:
            return os.remove("/tmp/" + self.filename)
        except Exception as e:
            logging.critical("Impossible de supprimer le fichier local: " + str(e))
            return False


def parse():
    """
    Définition de la ligne de commande
    """
    parser = argparse.ArgumentParser(
        prog="wildfly-cli", description="Interact with wildfly via web API"
    )
    # Nexus Repository Manager args
    parser.add_argument(
        "-u", "--download-url", help="URL to get the artifact", type=str
    )
    parser.add_argument("--url-login", help="URL user for authentification", type=str)
    parser.add_argument("--url-password", help="URL user's password", type=str)
    # Wildfly args
    parser.add_argument(
        "-w",
        "--wildfly",
        help="Wildfly's admin interface url, something like http://wildfly01.company.com:9990/",
        type=str,
        required=True,
    )
    parser.add_argument("-l", "--wildfly-login", help="Wildfly user", type=str)
    parser.add_argument(
        "-p", "--wildfly-password", help="Wildfly user's password", type=str
    )
    # Artifact args
    parser.add_argument(
        "-a",
        "--artifact",
        help="Artifact file name, something like 'ws-client01-1.0.0.war'. Unrequired if URL is specified.",
        type=str,
    )
    # Action
    parser.add_argument(
        "action",
        type=str,
        choices=[
            "deploy",
            "undeploy",
            "redeploy",
            "is_deployed",
            "isnt_deployed",
            "undeployed",
            "list_deployed",
        ],
        help="What to do with the specified artifact",
    )
    # Display
    parser.add_argument(
        "-v",
        "--verbose",
        help="Debug output level. Warning is '-v', Info is '-vv', Debug is '-vvv'",
        action="count",
    )
    return parser.parse_args()


def main():
    """
    Définition de la fonction principale. En python on ne fait pas ça directement dans le __main__ ou hors fonction.
    """
    # Parse CLI
    args = parse()

    # Logs
    if not args.verbose:
        log_level = logging.ERROR
    elif args.verbose == 1:
        log_level = logging.WARNING
    elif args.verbose == 2:
        log_level = logging.INFO
    elif args.verbose >= 3:
        log_level = logging.DEBUG
    logging.basicConfig(format="%(asctime)s %(levelname)s %(message)s", level=log_level)

    # Arguments
    success = False
    w = Wildfly(args.wildfly, args.wildfly_login, args.wildfly_password)
    if args.action != "list_deployed":
        try:
            a = Artifact(
                url=args.download_url, login=args.url_login, password=args.url_password
            )
        except Exception:
            a = Artifact(filename=args.artifact)

    # Actions
    if args.action == "deploy":
        success = a.download() and w.upload(a) and w.deploy(a) and w.is_deployed(a)

    elif args.action == "undeploy":
        success = w.undeploy(a) and w.remove(a) and not w.is_deployed(a)

    elif args.action == "is_deployed":
        success = w.is_deployed(a)

    elif args.action == "isnt_deployed" or args.action == "undeployed":
        success = not w.is_deployed(a)

    elif args.action == "redeploy":
        w.undeploy(a)
        w.remove(a)
        success = not w.is_deployed(a) and a.download() and w.upload(a) and w.deploy(a)

    elif args.action == "list_deployed":
        print(w.list_deployed())
        success = True

    # Return code
    if success:
        logging.info("Action " + args.action + " OK")
        return 0
    else:
        logging.critical("Action " + args.action + " KO !!!")
        return 1


if __name__ == "__main__":
    sys.exit(main())
