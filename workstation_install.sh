#!/usr/bin/env bash
# set -e

WKDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

sudo add-apt-repository -y universe
sudo add-apt-repository -y ppa:peek-developers/stable
sudo add-apt-repository -y ppa:nextcloud-devs/client

curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list'

sudo apt-get update -qq
sudo apt-get upgrade -y -qq

for p in apt-file \
  apt-transport-https \
  audacious \
  bat \
  ca-certificates \
  coreutils \
  curl \
  etckeeper \
  evince \
  exa \
  ffmpeg \
  fd-find \
  firefox \
  flameshot \
  fontconfig \
  fonts-firacode \
  git \
  golang \
  jq \
  libreoffice \
  meld \
  mlocate \
  msttcorefonts \
  neovim \
  nfs-kernel-server \
  nmap \
  nautilus-nextcloud \
  packer \
  peek \
  pgadmin4-desktop \
  screen \
  shellcheck \
  silversearcher-ag \
  snapd \
  software-properties-common \
  strace \
  terraform \
  ubuntu-restricted-extras \
  vagrant \
  vlc \
  xclip \
  xfonts-utils \
  xsel \
  zsh \
  zlib1g-dev; do
  sudo apt-get install -y $p
done

sudo etckeeper init

sudo snap install chromium
sudo snap install pgadmin4

# ZSH
if [ ! -f "$HOME/.zshrc" ]; then
  cp "$WKDIR/zshrc" "$HOME/.zshrc"
fi
if [ ! -f "/root/.zshrc" ]; then
  sudo cp "$WKDIR/zshrc" "/root/.zshrc"
fi
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Git
if [ ! -f "$HOME/.gitconfig" ]; then
  cp "$WKDIR/gitconfig" "$HOME/.gitconfig"
fi
if [ ! -f "$HOME/.gitignore" ]; then
  cp "$WKDIR/gitignore" "$HOME/.gitignore"
fi
if [ ! -f "/root/.gitconfig" ]; then
  sudo cp "$WKDIR/gitconfig" "/root/.gitconfig"
fi
if [ ! -f "/root/.gitignore" ]; then
  sudo cp "$WKDIR/gitignore" "/root/.gitignore"
fi

# GPG
# cat <<EOGPG >~/.gnupg/gpg.conf
# use-agent
# pinentry-mode loopback
# EOGPG
# echo "allow-loopback-pinentry" >~/.gnupg/gpg-agent.conf
# echo RELOADAGENT | gpg-connect-agent
# gpg --generate-key

# Python
sudo apt-get install -y python3 python3-pip tox
pip3 install -U \
  black \
  chime \
  cookiecutter \
  credstash \
  crudini \
  cryptography \
  flake8 \
  git-pull-request \
  gitlabber \
  glab \
  glances \
  httpie \
  isort \
  pre-commit \
  requests \
  rich-cli \
  tox-run-command \
  typer \
  youtube-dl \
  yq
# sh "$WKDIR/pyenv_install.sh"
# sh "$WKDIR/poetry_install.sh"

# Ansible
sudo pip3 install -U ansible molecule

# Node
sh "$WKDIR/node_install.sh"

# AWS
# pip3 install -U scoutsuite
sh "$WKDIR/awscli_install.sh"
sh "$WKDIR/awless_install.sh"

# Docker
curl -fsSL https://get.docker.com | sudo sh
sudo pip3 install -U docker docker-compose
sudo usermod -a -G docker "$(whoami)"
sudo systemctl disable docker

# Docker alternative
sh "$WKDIR/podman_install.sh"
sh "$WKDIR/buildah_install.sh"

# HashiCorp
# sh "$WKDIR/terraform_install.sh"
cp ./terraformrc ~/.terraformrc
sh "$WKDIR/terragrunt_install.sh"

# k8s
sh "$WKDIR/minikube_install.sh"
# sh "$WKDIR/minikube_configure_kvm_driver.sh"
sh "$WKDIR/kubectl_install.sh"
# sh "$WKDIR/istioctl_install.sh"
sh "$WKDIR/helm_install.sh"

# Openstack
# pip3 install python-openstackclient

# (Space)Vim
curl -sLf https://spacevim.org/install.sh | bash
mkdir -p "$HOME/.SpaceVim.d/autoload"
cp "$WKDIR/spacevim_init.toml" "$HOME/.SpaceVim.d/init.toml"
cp "$WKDIR/spacevim_myspacevim.vim" "$HOME/.SpaceVim.d/autoload/myspacevim.vim"
curl -sLf https://spacevim.org/install.sh | sudo bash
sudo mkdir -p "$HOME/.SpaceVim.d/autoload"
sudo cp "$WKDIR/spacevim_init.toml" "$HOME/.SpaceVim.d/init.toml"
sudo cp "$WKDIR/spacevim_myspacevim.vim" "$HOME/.SpaceVim.d/autoload/myspacevim.vim"

# VSCode
sh "$WKDIR/vscodium_install.sh"

# Doggo, dig alternative
sh "$WKDIR/doggo_install.sh"

# Cleaning
sudo apt-get remove -y audacity rhythmbox
sudo apt-get autoremove -y

# Indexing
sudo apt-file update
sudo updatedb
