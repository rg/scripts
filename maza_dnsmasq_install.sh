#!/usr/bin/env bash

# Maza
sudo cp /etc/hosts /etc/hosts.backup
curl -o maza https://raw.githubusercontent.com/tanrax/maza-ad-blocking/master/maza && chmod +x maza && sudo mv maza /usr/local/bin
sudo maza start

# DNSMasq
sudo apt-get install -y dnsmasq

cat <<EOF >/etc/dnsmasq.conf
# Don't use /etc/resolv.conf
no-resolv
# Use CloudFlare DNS
server=1.1.1.1
server=1.0.0.1
# Redirect those TLD to localhost
address=/.localhost/127.0.0.1

# Use maza config
conf-file=/root/.maza/dnsmasq.conf

# Don't bind on all interfaces, it messes up libvirt's own DNSMasq instances
bind-interfaces
interface=lo
EOF
# This will list real interfaces
for iface in $(find /sys/class/net/ -type l ! -lname '*/devices/virtual/net/*' -printf '%f '); do
  echo "interface=$iface" >>/etc/dnsmasq.conf
done

sudo systemctl restart dnsmasq
