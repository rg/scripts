#!/usr/bin/env bash

OPERATION=${1:-"create"}
CLUSTER=${2:-"arn:aws:ecs:eu-north-1:651142232517:cluster/quartr-development-data-ecs"}
PROVIDER=${3:-"EC2"}
ROLE=${4:-"arn:aws:iam::651142232517:role/quartr-development-data-ecs"}



if [ "$OPERATION" = "create" ] ; then
    aws ecs register-task-definition \
        --family ecs-test-td \
        --task-role-arn "$ROLE" \
        --execution-role-arn "$ROLE" \
        --container-definitions '[{"name":"ecs-test-container","image":"alpine","command":["sleep","infinity"],"cpu":128,"memory":128}]' 1> /dev/null

    aws ecs create-service \
        --cluster "$CLUSTER" \
        --service-name ecs-test-service \
        --task-definition ecs-test-td \
        --desired-count 1 \
        --capacity-provider-strategy "capacityProvider=$PROVIDER,weight=1" 1> /dev/null
fi



if [ "$OPERATION" = "status" ] ; then
    SV_ARN=$(aws ecs list-services --cluster "$CLUSTER" | jq -r '.serviceArns[] | select(.|contains("ecs-test-service"))')
    if [ "$SV_ARN" != "" ] ; then
        aws ecs describe-services --cluster "$CLUSTER" --services "$SV_ARN" | jq -r '.services[0].events[]|.message'
    fi
fi



if [ "$OPERATION" = "delete" ] ; then
    aws ecs delete-service --cluster "$CLUSTER" --service ecs-test-service --force 1> /dev/null
    TD_ARNS=$(aws ecs list-task-definitions | jq -r '.taskDefinitionArns[]|select(.|contains("ecs-test"))')
    for TD in $TD_ARNS ; do 
        aws ecs deregister-task-definition --task-definition "$TD" 1> /dev/null
        aws ecs delete-task-definitions --task-definitions "$TD" 1> /dev/null
    done
fi



if [ "$OPERATION" = "list" ] ; then
    echo "Task definitions:"
    aws ecs list-task-definitions | jq -r '"    " + .taskDefinitionArns[]'
    echo "Services:"
    aws ecs list-services --cluster "$CLUSTER" | jq -r '"    " + .serviceArns[]'
    echo "Container instances:"
    aws ecs list-container-instances --cluster "$CLUSTER" | jq -r '"    " + .containerInstanceArns[]'
    echo "Tasks:"
    aws ecs list-tasks --cluster "$CLUSTER" | jq -r '"    " + .taskArns[]'
fi



if [ "$OPERATION" = "run" ] ; then
  echo "Run task"
  TD=$(aws ecs list-task-definitions | jq -r '.taskDefinitionArns[]|select(.|contains("ecs-test"))')
  SUBNETS=$(aws ec2 describe-subnets | jq -r '.Subnets[]|select(.Tags[].Value|contains("public"))|.SubnetId' | sort -u | tr '\n' ',' | sed 's/,$//')
  # SG=$(aws ec2 describe-security-groups | jq -r '..|.GroupName? // empty|select(.|contains("ecs"))') # Beautiful but GroupName instead of GroupId
  SG=$(aws ec2 describe-security-groups | jq -r '.SecurityGroups[] | select(.GroupName | contains("ecs") ) | .GroupId')
  aws ecs run-task \
    --cluster $CLUSTER \
    --task-definition $TD \
    --network-configuration "awsvpcConfiguration={subnets=[$SUBNETS], securityGroups=[$SG], assignPublicIp=ENABLED}" 
    # --overrides '{"containerOverrides":[{"name":"task-worker","command":["bash", "-c", "apt-get update &>/dev/null ; apt-get install -y awscli &>/dev/null ; aws secretsmanager list-secrets --query Name ; aws secretsmanager get-secret-value --secret-id prod/quartromate/superadmin --query SecretString --output text"]}]}'
fi

# Login to the ASG EC2 instance, then exec in the above container


# Check the DB access
#     mysql -h quartr.csk4iwfqpnns.eu-north-1.rds.amazonaws.com -u quartr -p production



# Check the container gots the TaskDefinition "task role arn" 
#     [root@ip-10-3-12-13 ~]# docker exec -ti 60574dbf207f ash
#     / # env | grep AWS_
#     AWS_CONTAINER_CREDENTIALS_RELATIVE_URI=/v2/credentials/a3b41be3-2e63-4729-a921-f55b88a5dfc2
#     AWS_EXECUTION_ENV=AWS_ECS_EC2



# Test S3
#     touch test
#     for b in quartr-development-data-content-dashboards quartr-development-data-shore-transcripts ; do 
#        echo
#        aws s3 ls s3://$b/
#        aws s3 cp test s3://$b/
#        aws s3 ls s3://$b/
#        aws s3 rm s3://$b/test
#     done
#     rm test

# Test SQS. Requires apk add py3-boto3
# aws sqs list-queues | jq -r '.QueueUrls[]'
# python3 test_sqs.py send --queue-url https://eu-north-1.queue.amazonaws.com/651142232517/quartr-development-data-create-low-prio.fifo --message-group-id test --message test 
# python3 test_sqs.py receive --queue-url https://eu-north-1.queue.amazonaws.com/651142232517/quartr-development-data-create-low-prio.fifo  

# import argparse
# import boto3
# import time

# # Initialize SQS client
# sqs = boto3.client('sqs', region_name='eu-north-1')

# def send_message(queue_url, message_body, message_group_id=None):
#     try:
#         # Send message to SQS queue
#         if message_group_id:
#             response = sqs.send_message(
#                 QueueUrl=queue_url,
#                 MessageBody=message_body,
#                 MessageGroupId=message_group_id
#             )
#         else:
#             response = sqs.send_message(
#                 QueueUrl=queue_url,
#                 MessageBody=message_body
#             )
#         print("Message sent successfully. Message ID:", response['MessageId'])
#     except Exception as e:
#         print("Error:", e)

# def receive_messages(queue_url):
#     try:
#         # Receive messages from SQS queue
#         response = sqs.receive_message(
#             QueueUrl=queue_url,
#             MaxNumberOfMessages=1,
#             WaitTimeSeconds=20  # Long polling to reduce the number of empty responses
#         )

#         # Check if there are any messages
#         if 'Messages' in response:
#             for message in response['Messages']:
#                 # Process the message
#                 print("Received message:", message['Body'])
#                 # Delete the message from the queue
#                 sqs.delete_message(
#                     QueueUrl=queue_url,
#                     ReceiptHandle=message['ReceiptHandle']
#                 )
#         else:
#             print("No messages available")
#     except Exception as e:
#         print("Error:", e)

# def main():
#     parser = argparse.ArgumentParser(description="Send or receive messages from an SQS queue.")
#     subparsers = parser.add_subparsers(dest="command")

#     send_parser = subparsers.add_parser("send", help="Send a message to the SQS queue")
#     send_parser.add_argument("--queue-url", required=True, help="URL of the SQS queue")
#     send_parser.add_argument("--message", required=True, help="Message to be sent")
#     send_parser.add_argument("--message-group-id", help="Message group ID for FIFO queues")

#     receive_parser = subparsers.add_parser("receive", help="Receive messages from the SQS queue")
#     receive_parser.add_argument("--queue-url", required=True, help="URL of the SQS queue")

#     args = parser.parse_args()

#     if args.command == "send":
#         send_message(args.queue_url, args.message, args.message_group_id)
#     elif args.command == "receive":
#         receive_messages(args.queue_url)

# if __name__ == "__main__":
#     main()
