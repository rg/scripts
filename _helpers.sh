#!/usr/bin/env bash

github_get_latest_release() {
  curl --silent "https://github.com/$1/releases/latest" | sed 's#.*tag/\(.*\)\".*#\1#'
}
