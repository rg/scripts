#!/bin/bash

# Get a list of all EC2 instances
instance_ids=$(aws ec2 describe-instances --query 'Reservations[].Instances[].InstanceId' --output text)

# Loop through each instance ID
for instance_id in $instance_ids; do
    # Get the launch time and termination time for the instance
    instance_info=$(aws ec2 describe-instances --instance-ids "$instance_id")

    # Extract launch time and termination time from the instance info
    launch_time=$(echo "$instance_info" | jq -r '.Reservations[0].Instances[0].LaunchTime')
    termination_time=$(echo "$instance_info" | jq -r '.Reservations[0].Instances[0].StateTransitionReason' | grep -oP '(?<=terminated: )[^,]+')

    # Check if termination time is not available (instance is still running)
    if [ -z "$termination_time" ]; then
        current_time=$(date +%s)
        launch_timestamp=$(date -d "$launch_time" +%s)
        runtime_minutes=$(( (current_time - launch_timestamp) / 60))
        echo "Instance $instance_id is still running for $runtime_minutes minutes."
    else
        # Convert ISO 8601 time format to Unix timestamp
        launch_timestamp=$(date -d "$launch_time" +%s)
        termination_timestamp=$(date -d "$termination_time" +%s)

        # Calculate the runtime in minutes
        runtime_minutes=$(( (termination_timestamp - launch_timestamp) / 60))

        echo "Instance $instance_id was running for $runtime_minutes minutes."
    fi
done
