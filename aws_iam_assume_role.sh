#!/bin/bash

ARN=${1:-"arn:aws:iam::ACCOUNTID:role/ROLENAME"}
SESSION=${2:-"SESSIONNAME"}

export $(printf "AWS_ACCESS_KEY_ID=%s AWS_SECRET_ACCESS_KEY=%s AWS_SESSION_TOKEN=%s" \
$(aws sts assume-role \
--role-arn $ARN \
--role-session-name $SESSION \
--query "Credentials.[AccessKeyId,SecretAccessKey,SessionToken]" \
--output text))
