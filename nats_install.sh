#!/usr/bin/env bash
# set -e

sudo apt update -qq && sudo apt install -y -qq jq unzip

SERVER_VERSION=$1
if [ -z "$SERVER_VERSION" ]; then
    SERVER_VERSION=$(curl -s https://api.github.com/repos/nats-io/nats-server/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)
fi
echo
echo "Installing 'nats-server' $SERVER_VERSION"
echo
curl -sLO https://github.com/nats-io/nats-server/releases/download/$SERVER_VERSION/nats-server-${SERVER_VERSION}-linux-amd64.zip
unzip -q -j "nats-server-${SERVER_VERSION}-linux-amd64.zip" "*/nats-server"
rm "nats-server-${SERVER_VERSION}-linux-amd64.zip"
sudo mv nats-server /usr/local/bin/
nats-server --version

NSC_VERSION=$(curl -s https://api.github.com/repos/nats-io/nsc/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)
echo
echo "Installing 'nsc' $NSC_VERSION"
echo
curl -sLO https://github.com/nats-io/nsc/releases/download/$NSC_VERSION/nsc-linux-amd64.zip
unzip -q "nsc-linux-amd64.zip"
rm "nsc-linux-amd64.zip"
sudo mv nsc /usr/local/bin/
nsc --version

if command -v go >/dev/null; then
    echo
    echo "Installing 'nk' latest version compiling from sources"
    echo
    go install github.com/nats-io/nkeys/nk@latest
else
    echo
    echo "NOT installing 'nk' since it must be compiled and 'go' isn't available"
    echo
fi

https://github.com/nats-io/nsc/releases/download/2.6.1/nsc-linux-amd64.zip

CLI_VERSION=$(curl -s https://api.github.com/repos/nats-io/natscli/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)
echo
echo "Installing 'nats' $CLI_VERSION"
echo
curl -sLO https://github.com/nats-io/natscli/releases/download/$CLI_VERSION/nats-${CLI_VERSION/v/}-linux-amd64.zip
unzip -q -j "nats-${CLI_VERSION/v/}-linux-amd64.zip" "*/nats"
rm "nats-${CLI_VERSION/v/}-linux-amd64.zip"
sudo mv nats /usr/local/bin/
nats --version

TOP_VERSION=$(curl -s https://api.github.com/repos/nats-io/nats-top/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)
echo
echo "Installing 'nats-top' $TOP_VERSION"
echo
curl -sLO "https://github.com/nats-io/nats-top/releases/download/$TOP_VERSION/nats-top_${TOP_VERSION/v/}_linux_amd64.tar.gz"
tar -xf "nats-top_${TOP_VERSION/v/}_linux_amd64.tar.gz" nats-top
rm "nats-top_${TOP_VERSION/v/}_linux_amd64.tar.gz"
sudo mv nats-top /usr/local/bin/
nats-top --help
