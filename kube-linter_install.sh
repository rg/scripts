#!/usr/bin/env bash


# https://github.com/stackrox/kube-linter/releases/download/0.2.5/kube-linter-linux.tar.gz

VERSION=$(curl -s https://api.github.com/repos/stackrox/kube-linter/git/refs/tags | jq -r '.[].ref' | grep -v -E 'rc|dev|beta' | cut -d'/' -f3 | sort --version-sort | tail -n1)

curl --progress-bar -LO "https://github.com/stackrox/kube-linter/releases/download/$VERSION/kube-linter-linux.tar.gz"
tar -xf kube-linter*.tar.gz 
sudo mv kube-linter /usr/local/bin/
kube-linter version
rm -f kube-linter*.tar.gz
