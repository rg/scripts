#!/usr/bin/env bash
set -xe

LATEST_VERSION=$(curl --silent "https://api.github.com/repos/gruntwork-io/terragrunt/releases/latest" | jq -r .tag_name)
VERSION=${1:-${LATEST_VERSION/v/}}

sudo curl -s -o /usr/local/bin/terragrunt -L https://github.com/gruntwork-io/terragrunt/releases/download/v$VERSION/terragrunt_linux_amd64
sudo chmod +x /usr/local/bin/terragrunt
terragrunt --version
