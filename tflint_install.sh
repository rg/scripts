#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/terraform-linters/tflint/git/refs/tags | jq -r '.[].ref' | grep -v rc | cut -d'/' -f3 | sort --version-sort | tail -n1)
curl -LO https://github.com/terraform-linters/tflint/releases/download/$VERSION/tflint_linux_amd64.zip
unzip tflint*.zip
sudo mv tflint /usr/local/bin/
rm -rf tflint*.zip
