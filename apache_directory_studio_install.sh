#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/apache/directory-studio/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | tail -n1)
curl --progress-bar -LO "https://dlcdn.apache.org/directory/studio/$VERSION/ApacheDirectoryStudio-$VERSION-linux.gtk.x86_64.tar.gz"
tar -xf "ApacheDirectoryStudio-$VERSION-linux.gtk.x86_64.tar.gz"
rm -f "ApacheDirectoryStudio-$VERSION-linux.gtk.x86_64.tar.gz"
sudo mv ApacheDirectoryStudio /usr/lib/
sudo ln -sf /usr/lib/ApacheDirectoryStudio/ApacheDirectoryStudio /usr/local/bin/ApacheDirectoryStudio
ApacheDirectoryStudio
