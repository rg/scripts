#!/usr/bin/env bash
set -euo pipefail

# download latest replibyte archive for Linux
curl -s https://api.github.com/repos/Qovery/replibyte/releases/latest | \
jq -r '.assets[].browser_download_url' | \
grep -i 'linux-musl.tar.gz$' | wget -qi - && \

# unarchive
tar zxf replibyte*.tar.gz
rm replibyte*.tar.gz

# make replibyte executable
chmod +x replibyte

# make it accessible from everywhere
sudo mv replibyte /usr/local/bin/

replibyte --version
