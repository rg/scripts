#!/usr/bin/env bash
# Migrate all images from a docker registry to another
# Copy/pasted from https://medium.com/@pjbgf/moving-docker-images-from-one-container-registry-to-another-2f1f1631dc49

SOURCE="$1"
TARGET="$2"
FILTER_OUT="${3:-rc|beta|alpha}"

# Download all images
docker pull "$SOURCE" --all-tags

# Get all images published after $SINCE_TAG
# format output to be:
#   docker tag SOURCE_NAME:VERSION TARGET_IMAGE_NAME:VERSION |
#   docker push TARGET_IMAGE_NAME:VERSION
# then filter the result, removing any entries containing words defined on $FILTER_OUT (i.e. rc, beta, alpha, etc)
# finally, execute those as commands
docker images "$SOURCE" \
  --format "docker tag {{.Repository}}:{{.Tag}} $TARGET:{{.Tag}} | docker push $TARGET:{{.Tag}}" |
  grep -vE $FILTER_OUT |
  bash
