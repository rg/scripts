#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/derailed/k9s/git/refs/tags | jq -r '.[].ref' | grep -v -E 'rc|dev|beta' | cut -d'/' -f3 | sort --version-sort | tail -n1)

curl --progress-bar -LO "https://github.com/derailed/k9s/releases/download/$VERSION/k9s_Linux_x86_64.tar.gz"
tar -xf k9s*.tar.gz k9s
sudo mv k9s /usr/local/bin/
k9s version
rm -f k9s*.tar.gz
