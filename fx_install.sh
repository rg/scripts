#!/usr/bin/env bash
set -euo pipefail

VERSION=24.0.0
sudo curl -sL "https://github.com/antonmedv/fx/releases/download/$VERSION/fx_linux_amd64" -o /usr/local/bin/fx
sudo chmod +x /usr/local/bin/fx
"/usr/local/bin/fx" --version
