#!/usr/bin/env bash

{
  kubectl get deployment -n backend -o jsonpath='{$..metadata.name}'
  kubectl get deployment -n frontend -o jsonpath='{$..metadata.name}'
  kubectl get deployment -n backend -o jsonpath='{$..status.replicas}'
  kubectl get deployment -n frontend -o jsonpath='{$..status.replicas}'
} | tr ' ' '\n' | cut -d '/' -f3 | sort -u | tr ':' ';' | sed 's;gitlab.*$;;' | tee replicas.csv
