#!/usr/bin/env bash

# Script to create a working directory, move there, plus git init & push
# Intended for small stuff, aka solving issues or playing with new techs

# To be moved to the directory, add an alias to ~/.zshrc or ~/.bashrc
#   alias working-on="source $HOME/bin/gs-working-on.sh"

set -e
# set -x

ID=$1
LOCAL="$HOME/work/gostudent/remy.garrigue/$(echo -n $ID | tr '[:upper:]' '[:lower:]')"
REMOTE=https://gitlab.gostudent.cloud/remy.garrigue/$ID

mkdir -p $LOCAL
cd $LOCAL

if [ ! -f ./README.md ]
then

cat <<EORM > README.md
# $ID

Working on $ID
EORM

cat <<EOPC > .pre-commit-config.yaml
---
# See https://pre-commit.com for more information
# See https://pre-commit.com/hooks.html for more hooks
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.3.0
    hooks:
      - id: trailing-whitespace
      - id: end-of-file-fixer
      - id: check-json
      - id: check-yaml
      - id: check-added-large-files
EOPC
 
touch .gitignore

{
  git init .
  git branch -M main 
  git add .
  git commit -m "Initial commit"
  git remote add origin $REMOTE
  git push --set-upstream origin main
  # glab repo view --web
} &>/dev/null

fi

