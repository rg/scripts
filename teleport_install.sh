#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/gravitational/teleport/git/refs/tags | jq -r '.[].ref' | grep -v -E 'rc|dev|beta' | cut -d'/' -f3 | sort --version-sort | tail -n1)

curl --progress-bar -LO https://get.gravitational.com/teleport-$VERSION-linux-amd64-bin.tar.gz
tar -xf teleport*.tar.gz
sudo mv teleport/teleport /usr/local/bin/
sudo mv teleport/tctl /usr/local/bin/
sudo mv teleport/tsh /usr/local/bin/
rm -rf teleport*.tar.gz teleport
echo "teleport version : $(teleport version)"
echo "tctl version : $(tctl version)"
echo "tsh version : $(tsh version)"
