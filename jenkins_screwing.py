#!/usr/bin/env python

from jenkinsapi.jenkins import Jenkins
import click
import urllib3


urllib3.disable_warnings()
ic = None


@click.group()
@click.option("-j", "--jenkins-url", "jenkins", default="https://jenkins.local")
@click.option("-u", "--user", default="rg")
@click.option("-p", "--password")
def cli(jenkins, user, password):
    global ic
    ic = Jenkins(jenkins, user, password, ssl_verify=False)


@click.command()
@click.argument("jobs", nargs=-1)
def lgb(jobs):
    global ic
    for j in jobs:
        job = ic[j]
        lgb = job.get_last_good_build()
        return lgb.get_revision()


cli.add_command(lgb)

if __name__ == "__main__":
    cli()
