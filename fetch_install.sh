#!/usr/bin/env bash
set -e

VERSION=$(curl -s https://api.github.com/repos/gruntwork-io/fetch/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)
curl -LO https://github.com/gruntwork-io/fetch/releases/download/$VERSION/fetch_linux_amd64
chmod +x fetch_linux_amd64
sudo mv fetch_linux_amd64 /usr/local/bin/fetch
