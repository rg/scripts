#!/usr/bin/env bash
set -e

. ./_helpers.sh

REPOSITORY="imsnif/bandwhich"
VERSION=$(github_get_latest_release $REPOSITORY)
curl --progress-bar -LO "https://github.com/$REPOSITORY/releases/download/$VERSION/bandwhich-v$VERSION-x86_64-unknown-linux-musl.tar.gz"
tar -xf bandwhich*.tar.gz
sudo mv bandwhich /usr/local/bin/
rm -rf bandwhich*.tar.gz
bandwhich --version
