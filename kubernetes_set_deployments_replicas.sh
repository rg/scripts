#!/usr/bin/env bash

DEPLOYMENTS=$HOME/work/gostudent/infrastructure/deployments/product-prod/services

for service in $(cat replicas.csv)
do
  name=$(echo $service | cut -d ';' -f 1)
  replicas=$(echo $service | cut -d ';' -f 2)
  file=$DEPLOYMENTS/$name/config.yaml 
  
  echo
  echo "- service $name"
  echo "  file $file"

  if [[ ($replicas -eq 1) && ("$name" != "citibank-service") ]]; then
    replicas=2
    echo "  replicas=2 (enforced)"
  else
    echo "  replicas=$replicas"
  fi

  if [ -f $file ]
  then    
    if ! grep -E -q "replicas:\s+$replicas" $file
    then
      echo "  setting..."
      sed -i "s;replicas:.*;replicas: $replicas;" $file
      if [ $? -ne 0 ]; then
        echo "  ko, sed failed, replicas look like:"
        echo -n "  "
        grep replicas $file
      else
        echo "  ok"
      fi
    else
      echo -n "  already ok "
      printf '\U2714'
      echo
    fi
  else
    echo "  !!! file doesn't exist !!!" 
  fi
done

