#!/bin/bash

# List of instance IDs
instances=("i-0b13a776e25c795a6" "i-0bc96762f2ed8e798" "i-0e9acd50f2917e131" "i-0ec2b8ff682ef3346" "i-017c80f4c5e2e7950")

# Loop through each instance
for i in "${instances[@]}"; do
    echo "Instance ID: $i"
    ssh "$i" "for pid in \$(pgrep -f runc); do echo 'Container PID: \$pid'; ip netns exec \$pid ip addr | grep 'inet ' | awk '{print \$2}' | cut -d'/' -f1; echo '-------------------'; done"
    echo
done
