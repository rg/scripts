#!/usr/bin/env bash

URL="https://github.com/$(curl -s https://github.com/mozilla/geckodriver/releases | grep download | grep linux64.tar.gz | head -n1 | cut -d'"' -f2)"
curl -sL "${URL}" | sudo tar -xz -C /usr/local/bin
geckodriver --version

