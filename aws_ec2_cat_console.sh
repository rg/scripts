#!/usr/bin/env bash

if [ $# -ne 1 ]; then
  echo "Display console logs of the FIRST running EC2 instance matching this Name tag"
  echo
  echo "Usage: $0 name"
  echo
  echo "  name:     Part of the name tag. Example, 'consul' will match instances with Name=entreprise-development-consul"
  exit 1
fi

NAME=${1}
ID=$(awless list instances --filter state=running,name="${NAME}" --format json | jq -r '.[0].ID')
AZ=$(awless list instances --filter state=running,name="${NAME}" --format json | jq -r '.[0].AvailabilityZone')

aws --region "${AZ::-1}" ec2 get-console-output --instance-id "${ID}" | jq -r '.Output'
