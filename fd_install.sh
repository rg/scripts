#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/sharkdp/fd/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)
curl -LO https://github.com/sharkdp/fd/releases/download/$VERSION/fd-$VERSION-x86_64-unknown-linux-gnu.tar.gz
tar -xf fd*.tar.gz
sudo mv fd-$VERSION-x86_64-unknown-linux-gnu/fd /usr/local/bin/
rm -rf fd-$VERSION-x86_64-unknown-linux-gnu*
