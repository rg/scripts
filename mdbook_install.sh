#!/usr/bin/env bash

VERSION=$(curl -s https://api.github.com/repos/rust-lang/mdBook/git/refs/tags | jq -r '.[].ref' | grep -v rc | cut -d'/' -f3 | sort --version-sort | tail -n1)
curl -LO "https://github.com/rust-lang/mdBook/releases/download/${VERSION}/mdbook-${VERSION}-x86_64-unknown-linux-gnu.tar.gz"
tar -xf mdbook*.tar.gz
sudo mv mdbook /usr/local/bin/
rm mdbook*.tar.gz
