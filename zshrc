if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
export PATH=$HOME/bin:$HOME/.local/bin:/usr/local/bin:$PATH
export PATH=$PATH:$HOME/.arkade/bin/

export ZSH="/home/remy/.oh-my-zsh"
# export ZSH_THEME="muse"
export ZSH_THEME="powerlevel10k/powerlevel10k"

export HYPHEN_INSENSITIVE="true"
export DISABLE_UPDATE_PROMPT="true"
export UPDATE_ZSH_DAYS=13
export ENABLE_CORRECTION="false"
export HIST_STAMPS="yyyy-mm-dd"

plugins=(git colored-man-pages kubectl fzf)
source $ZSH/oh-my-zsh.sh

# if [[ -n $SSH_CONNECTION ]]; then
export EDITOR='vim'
# else
#   export EDITOR='codium'
# fi

bindkey '\e[5~' history-beginning-search-backward
bindkey '\e[6~' history-beginning-search-forward
bindkey '\e[A' history-beginning-search-backward
bindkey '\e[B' history-beginning-search-forward

alias adif="awless delete instance -f id="
alias arc="all-repos-clone -C ~/.all-repos.json"
alias bat="batcat"
alias c="codium"
alias cat="bat -pp"
alias d="doggo"
alias e="exa -la"
alias f="fdfind"
alias fd="fdfind"
alias glo="git log --oneline --decorate --reverse"
alias gpt="git push --tags --force"
alias gt="git tag"
alias mk="minikube kubectl -- "
alias pcra="pre-commit run -a"
alias t="exa --tree"
alias tree="exa --tree"
alias tf="terraform"
alias tg="terragrunt"
alias tga="terragrunt apply -auto-approve"
alias tgd="terragrunt destroy -auto-approve"
alias tgp="terragrunt plan"
alias tgia="terragrunt run-all init -upgrade --terragrunt-non-interactive"
alias tgpa="terragrunt run-all plan --terragrunt-non-interactive"
alias tgaa="terragrunt run-all apply --terragrunt-non-interactive"
alias uua="sudo apt update ; sudo apt upgrade -y ; sudo apt autoremove -y"
alias vd="vagrant destroy -f"
alias vu="vagrant up"
alias ytdl="youtube-dl -x --audio-format mp3"

# Golang
export PATH=$PATH:$HOME/go/bin

# Terraform
export TF_PLUGIN_CACHE_DIR=$HOME/.cache/terraform_plugin_cache
export TF_IN_AUTOMATION=true
mkdir -p $TF_PLUGIN_CACHE_DIR

# Ansible
export ANSIBLE_HOST_KEY_CHECKING=False

# Molecule
# if command -v molecule &>/dev/null; then
#   eval "$(_MOLECULE_COMPLETE=source molecule)"
# fi

# NVM
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
# nvm use --silent 11

# AWS CLI 2
autoload bashcompinit && bashcompinit
complete -C '/usr/local/bin/aws_completer' aws
export AWS_PAGER="jq"

# Awless for AWS
if command -v awless &>/dev/null; then
  source <(awless completion zsh)
  alias ali='awless list instances --sort "name,id" --columns "id,name,private ip,public ip,state,uptime"'
  # Wazo
  # alias lab='awless switch us-lab ; awless list instances --sort "name,id" --columns "id,name,private ip,public ip,state,uptime"'
  # alias lab-eu='awless switch eu-lab ; awless list instances --sort "name,id" --columns "id,name,private ip,public ip,state,uptime"'
  # alias demo='awless switch eu-demo ; awless list instances --sort "name,id" --columns "id,name,private ip,public ip,state,uptime"'
  # alias development='awless switch eu-dev ; awless list instances --sort "name,id" --columns "id,name,private ip,public ip,state,uptime"'
  # alias staging='awless switch eu-stg ; awless list instances --sort "name,id" --columns "id,name,private ip,public ip,state,uptime"'
  # alias production='awless switch na-prod ; awless list instances --sort "name,id" --columns "id,name,private ip,public ip,state,uptime"'
  # alias production-eu='awless switch eu-prod ; awless list instances --sort "name,id" --columns "id,name,private ip,public ip,state,uptime"'
fi

# aws-sso to easilly switch between accounts
if command -v aws-sso &>/dev/null; then
  autoload -U +X bashcompinit && bashcompinit
  complete -o nospace -C /usr/bin/aws-sso aws-sso
  # GoStudent
  alias gs-aws-master='eval $(unset KUBECONFIG ; aws-sso eval --account 004924675112 --role aws_sso_terraform_plan)'
  alias gs-aws-data-ops='eval $(unset KUBECONFIG ; aws-sso eval --account 930997837099 --role Admin)'
  alias gs-aws-data-dev='eval $(unset KUBECONFIG ; aws-sso eval --account 986812996597 --role Admin) && aws eks --region eu-central-1 update-kubeconfig --name gs-data-dev-eks 1>/dev/null'
  alias gs-aws-data-prod='eval $(unset KUBECONFIG ; aws-sso eval --account 946167535447 --role Admin) && aws eks --region eu-central-1 update-kubeconfig --name gs-data-prod-eks 1>/dev/null'
  alias gs-aws-data-prod-team='eval $(unset KUBECONFIG ; aws-sso eval --account 946167535447 --role DataTeam)'
  alias gs-aws-network='eval $(unset KUBECONFIG ; aws-sso eval --account 493570361862 --role Admin)'
  alias gs-aws-product-ops='eval $(unset KUBECONFIG ; aws-sso eval --account 428037846224 --role Admin)'
  alias gs-aws-product-dev-new='eval $(unset KUBECONFIG ; aws-sso eval --account 101476298835 --role Admin) && aws eks --region eu-central-1 update-kubeconfig --name gs-product-dev-eks 1>/dev/null'
  alias gs-aws-product-dev='eval $(unset KUBECONFIG ; aws-sso eval --account 101476298835 --role Admin) && aws eks --region eu-central-1 update-kubeconfig --name eks-product-dev-eu-central-1 1>/dev/null'
  alias gs-aws-product-stage='eval $(unset KUBECONFIG ; aws-sso eval --account 38565832015 --role Admin) && aws eks --region eu-central-1 update-kubeconfig --name gs-product-stage-eks 1>/dev/null'
  alias gs-aws-product-qa='eval $(unset KUBECONFIG ; aws-sso eval --account 75276055047 --role Admin && aws eks --region eu-central-1 update-kubeconfig --name blackpearl-product-qa-eu-central-1 1>/dev/null)'
  alias gs-aws-product-prod='eval $(unset KUBECONFIG ; aws-sso eval --account 213897467017 --role Admin && aws eks --region eu-central-1 update-kubeconfig --name gs-product-prod-eks 1>/dev/null)'
  alias gs-aws-shared-services='eval $(unset KUBECONFIG ; aws-sso eval --account 728080024150 --role Admin) && aws eks --region eu-central-1 update-kubeconfig --name eks-shared-services-eu-central-1 1>/dev/null'
  alias gs-hetzner-dev='export KUBECONFIG=/home/remy/work/gostudent/remy.garrigue/hetzner/dev.yaml '
  alias gs-hetzner-dev0='export KUBECONFIG=/home/remy/work/gostudent/remy.garrigue/hetzner/dev0.yaml '
  alias gs-hetzner-dev1='export KUBECONFIG=/home/remy/work/gostudent/remy.garrigue/hetzner/dev1.yaml '
  alias gs-hetzner-dev2='export KUBECONFIG=/home/remy/work/gostudent/remy.garrigue/hetzner/dev2.yaml '
  alias gs-hetzner-prodnew='export KUBECONFIG=/home/remy/work/gostudent/remy.garrigue/hetzner/prod-new.yaml '
  alias gs-hetzner-prodold='export KUBECONFIG=/home/remy/work/gostudent/remy.garrigue/hetzner/prod-old.yaml '
fi

# GoStudent workdirs
alias charts="cd ~/work/gostudent/infrastructure/eks-helm-charts"
alias deployments="cd ~/work/gostudent/infrastructure/deployments"
alias depl-data="cd ~/work/gostudent/infrastructure/deployments-data"
alias depl-product="cd ~/work/gostudent/infrastructure/deployments-product"
alias depl-infra="cd ~/work/gostudent/infrastructure/deployments-infrastructure"
alias grunt="cd ~/work/gostudent/infrastructure/aws-infra/terragrunt"
alias wk="cd ~/work/gostudent/remy.garrigue"

# Cloudflare
export CLOUDFLARE_API_TOKEN=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

# Packer
# if command -v packer &>/dev/null; then
#   autoload -U +X bashcompinit && bashcompinit
#   complete -o nospace -C /usr/bin/packer packer
# fi

# pyenv
# if [ -d $HOME/.pyenv/ ]; then
#   export PATH="$HOME/.pyenv/bin:$PATH"
#   eval "$(pyenv init -)"
# fi

# Minikube
if command -v minikube &>/dev/null; then
  source <(minikube completion zsh)
fi

# Kubectl
if command -v kubectl &>/dev/null; then
  source <(kubectl completion zsh)
  alias kg="kubectl get"
  alias kd="kubectl describe"
  export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
fi

# Istioctl
# if command -v istioctl &>/dev/null; then
#   source <(istioctl completion zsh)
# fi

# Helm
if command -v helm &>/dev/null; then
  source <(helm completion zsh)
fi

# ArgoCD
if command -v argocd &>/dev/null; then
  alias a="argocd"
  source <(argocd completion zsh)
  export ARGOCD_OPTS=--grpc-web
fi

# all repos
if command -v all-repos-complete &>/dev/null; then
  eval "$(all-repos-complete -C ~/.all-repos.json --zsh)"
fi

# Github CLI
# if command -v gh &>/dev/null; then
#   source <(gh completion -s zsh)
# fi

# Gitlab CLI
if command -v glab &>/dev/null; then
  source <(glab completion -s zsh)
  alias glmrcm="glab mr create -f -y --remove-source-branch -b master"
  alias glmrcd="glab mr create -f -y --remove-source-branch -b dev"
  alias glmrv="glab mr view --web"
  alias glrv="glab repo view --web"
fi

# Velero backups
if command -v velero &>/dev/null; then
  source <(velero completion zsh)
fi

# GoLang
# go env -w GOPRIVATE="gitlab.gostudent.cloud"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
