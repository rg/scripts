#!/usr/bin/env python3
# !!! MANAGED BY ANSIBLE !!!

import logging
from typing import List

import boto3
import click
import requests
from systemd.journal import JournalHandler


def get_aws_region() -> str:
    """Get executing EC2 instance region through EC2 internal endpoint"""
    r = requests.get("http://169.254.169.254/latest/dynamic/instance-identity/document")
    response_json = r.json()
    return response_json.get("region")


def _get_ec2_instance_public_ip(instance_id: str) -> str:
    """Get executing EC2 instance Public IP through EC2 API"""
    result = (
        ec2.describe_instances(InstanceIds=[instance_id])
        .get("Reservations")[0]
        .get("Instances")[0]
        .get("PublicIpAddress")
    )

    log.debug(f"AWS EC2 public IP for '{instance_id}' is '{result}'")
    return result


def _get_route53_parent_zone_id(record: str) -> str:
    """Get the AWS Route53 parent Zone ID for the given domain"""
    zone = '.'.join(record.split('.')[1:])
    zone_id = r53.list_hosted_zones_by_name(DNSName=zone)['HostedZones'][0]['Id']
    log.debug(
        f"DNS record '{record}' is in zone '{zone}' whose AWS Route53 ID is '{zone_id}'"
    )
    return zone_id


def get_consul_service_healthy_instance(consul: str, service: str) -> List[str]:
    """Get the healthy (all check passing) instances name for the given Consul's service"""
    result = []
    nodes_list = [
        service["Node"]
        for service in requests.get(f"{consul}/catalog/service/{service}").json()
    ]
    for node in nodes_list:
        statuses = [
            node["Status"]
            for node in requests.get(f"{consul}/health/node/{node}").json()
        ]
        node_status = "passing"
        for status in statuses:
            if status != "passing":
                node_status = "failing"

        if node_status == "passing":
            result.append(node)

    log.debug(
        f"Healthy instances for Consul '{consul}' service '{service}' are '{', '.join(result)}'"
    )
    return result


def _upsert_route53_record(record: str, values: List[str], ttl: int = 30) -> bool:
    """Update or insert (create) a DNS A record in AWS Route53 (DNS) with the given IPs as values"""
    result = False
    try:
        zone_id = _get_route53_parent_zone_id(record)
        response = r53.change_resource_record_sets(
            HostedZoneId=zone_id,
            ChangeBatch={
                'Comment': 'Updating record according to Consul healthy instances',
                'Changes': [
                    {
                        'Action': 'UPSERT',
                        'ResourceRecordSet': {
                            'Name': record,
                            'Type': 'A',
                            'TTL': ttl,
                            'ResourceRecords': [{'Value': ip} for ip in values],
                        },
                    },
                ],
            },
        )
        result = response['ChangeInfo']['Status'] == 'PENDING'
    except Exception as e:
        log.error(e)

    return result


@click.command()
@click.option("-s", "--service", type=str, required=True, help="Consul service name")
@click.option(
    "-r",
    "--record",
    type=str,
    required=True,
    help="Record or hostname to update in AWS Route53",
)
@click.option(
    "-c", "--consul", type=str, default="http://localhost:8500/v1", help="Consul URL"
)
@click.option(
    "--debug", is_flag=True, default=False, help="Enable debug mode", hidden=True
)
def main(
    service: str,
    record: str,
    consul: str,
    debug: bool = False,
):
    global ec2, r53, log

    log = logging.getLogger(__name__)
    log.addHandler(JournalHandler())
    if debug:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    aws_region = get_aws_region()
    ec2 = boto3.client("ec2", region_name=aws_region)
    instances = get_consul_service_healthy_instance(consul, service)

    public_ips = [_get_ec2_instance_public_ip(instance) for instance in instances]

    r53 = boto3.client("route53", region_name=aws_region)

    success = _upsert_route53_record(record, public_ips)

    if success:
        log.info(
            f"Updating AWS Route53 '{record}' record with Consul service '{service}' healthy instances public IPs '{', '.join(public_ips)}'"
        )
    else:
        log.error(
            f"Failed updating AWS Route53 '{record}' record values with IPs '{', '.join(public_ips)}'"
        )


if __name__ == "__main__":
    main()
