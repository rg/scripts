#!/usr/bin/env bash
set -e

VERSION=$(curl -s https://api.github.com/repos/wagoodman/dive/git/refs/tags | jq -r '.[].ref' | cut -d'/' -f3 | sort --version-sort | tail -n1)

curl -LO "https://github.com/wagoodman/dive/releases/download/v${VERSION}/dive_${VERSION}_linux_amd64.deb"
sudo apt install -y "./dive_${VERSION}_linux_amd64.deb"
rm -f "./dive_${VERSION}_linux_amd64.deb"
