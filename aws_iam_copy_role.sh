#!/bin/bash

# Variables
EXISTING_ROLE_NAME="$1"
NEW_ROLE_NAME="$2"

# Step 1: Get the existing role's trust policy
echo "Fetching trust policy for role: $EXISTING_ROLE_NAME"
aws iam get-role --role-name "$EXISTING_ROLE_NAME" --query 'Role.AssumeRolePolicyDocument' --output json > trust-policy.json

# Step 2: Create a new role with the same trust policy
echo "Creating new role: $NEW_ROLE_NAME"
aws iam create-role --role-name "$NEW_ROLE_NAME" --assume-role-policy-document file://trust-policy.json

# Step 3: Get the existing role's attached policies
echo "Fetching attached policies for role: $EXISTING_ROLE_NAME"
aws iam list-attached-role-policies --role-name "$EXISTING_ROLE_NAME" --query 'AttachedPolicies[*].PolicyArn' --output text > policies.txt

# Step 4: Attach the same policies to the new role
echo "Attaching policies to new role: $NEW_ROLE_NAME"
while read -r policy; do
  aws iam attach-role-policy --role-name "$NEW_ROLE_NAME" --policy-arn "$policy"
done < policies.txt

# Cleanup
rm trust-policy.json policies.txt

echo "Role copied successfully from $EXISTING_ROLE_NAME to $NEW_ROLE_NAME."
