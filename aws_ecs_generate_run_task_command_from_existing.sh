#!/bin/bash
# Start an AWS ECS Task using the UI, then use this script to generate the command that will run the same. For AWS ECS tests purpose

# Set ECS cluster name
CLUSTER_NAME=$(aws ecs list-clusters --output text --query 'clusterArns[0]' | cut -d'/' -f2)

# Fetch the ARN of the running task
RUNNING_TASK_ARN=$(aws ecs list-tasks --cluster $CLUSTER_NAME --output text --query 'taskArns[0]')

# Fetch the task definition ARN from the running task
TASK_DEFINITION_ARN=$(aws ecs describe-tasks --cluster $CLUSTER_NAME --tasks $RUNNING_TASK_ARN --output text --query 'tasks[0].taskDefinition')

# Fetch the subnet ID from the running task
SUBNET_ID=$(aws ecs describe-tasks --cluster $CLUSTER_NAME --tasks $RUNNING_TASK_ARN --output text --query 'tasks[0].attachments[0].details[?name==`subnet-id`].value')

# Fetch the security group IDs from the running task
SECURITY_GROUP_IDS=$(aws ecs describe-tasks --cluster $CLUSTER_NAME --tasks $RUNNING_TASK_ARN --output text --query 'tasks[0].attachments[0].details[?name==`security-group-id`].value' | tr '\t' ',')

# Set any additional options you need
ADDITIONAL_OPTIONS="--launch-type FARGATE --platform-version LATEST"

# Generate the aws ecs run-task command
aws_command="aws ecs run-task \
    --cluster $CLUSTER_NAME \
    --task-definition $TASK_DEFINITION_ARN \
    --network-configuration 'awsvpcConfiguration={subnets=[$SUBNET_ID],securityGroups=[$SECURITY_GROUP_IDS],assignPublicIp=ENABLED}' \
    $ADDITIONAL_OPTIONS"
# --overrides '{\"containerOverrides\":[{\"name\":\"<container-name>\",\"command\":[\"<command-to-run>\"]}]}' \
echo "$aws_command"

# Uncomment the line below to execute the command
# eval "$aws_command"
