#!/usr/bin/#!/usr/bin/env python3

# pip install --upgrade python-gitlab
# http://python-gitlab.readthedocs.io/en/stable/

import gitlab
import requests
import urllib3

session = requests.Session()
session.verify = False
urllib3.disable_warnings()

gl = gitlab.Gitlab(
    "https://gitlab.local",
    private_token="XXXXXXXXXXXYYYYYZZ",
    api_version=4,
    session=session,
)

myself = gl.users.list(username="rg")[0]

# Create a new pipeline for every project with a failed pipeline
for userproject in myself.projects.list(all=True):
    # for project in gl.projects.owned():
    project = gl.projects.get(userproject.get_id())
    if project.pipelines.list():
        last_pipeline = project.pipelines.list()[-1]
        if last_pipeline.status == "failed":
            # New pipeline
            print("%s : triggering pipeline" % project.name)
            project.pipelines.create({"ref": "master"})
        else:
            print(
                "%s : last pipeline is %s, skipping"
                % (project.name, last_pipeline.status)
            )
    else:
        print("%s : no existing pipeline, skipping" % project.name)
