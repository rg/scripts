#!/usr/bin/env bash
#
# Self assign an existing EIP
#
# Requirements:
#   apt-get update
#   apt-get install -y awscli curl jq
set -e

EIP_ALLOCATION_ID="$1"

echo
echo "Getting instance id"
id=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
export id

echo
echo "Getting region"
region=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r '.region')
export region

echo
echo "Assigning Elastic IP to this instance"
aws --region "${region}" ec2 associate-address --instance-id "${id}" --allocation-id "${EIP_ALLOCATION_ID}" --allow-reassociation
