#!/bin/bash

# Set the PVC name and namespace
PVC_NAME="$2"
NAMESPACE="$1"

# Remove the protection termination finalizer from the PVC
kubectl patch pvc -n $NAMESPACE $PVC_NAME -p '{"metadata": {"finalizers": null}}' --type=merge

# Remove the PVC
kubectl delete pvc -n $NAMESPACE $PVC_NAME

# # Get the PV name from the PVC
# PV_NAME=$(kubectl get pvc -n $NAMESPACE $PVC_NAME -o jsonpath='{.spec.volumeName}')

# # Remove the PV
# kubectl delete pv $PV_NAME
