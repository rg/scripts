#!/bin/bash

# Define the pattern to match subnets
PATTERN="$1"  # Replace with your actual pattern

# Get all subnets and filter by the pattern
SUBNET_IDS=$(aws ec2 describe-subnets --query "Subnets[?contains(CidrBlock, \`${PATTERN}\`)].SubnetId" --output text)

# Loop through each matching subnet
for SUBNET_ID in $SUBNET_IDS; do
    # Get the CIDR block for the current subnet
    CIDR_BLOCK=$(aws ec2 describe-subnets --subnet-ids "$SUBNET_ID" --query "Subnets[0].CidrBlock" --output text)

    # Calculate total usable IP addresses
    IFS='/' read -r base mask <<< "$CIDR_BLOCK"
    TOTAL_IPS=$((2 ** (32 - mask)))
    USABLE_IPS=$((TOTAL_IPS - 5))  # Subtracting 5 for network, broadcast, and reserved addresses

    # Count used IP addresses
    USED_IPS=$(aws ec2 describe-instances --filters "Name=subnet-id,Values=$SUBNET_ID" --query "Reservations[*].Instances[*].PrivateIpAddress" --output text | wc -w)

    # Calculate available IP addresses
    AVAILABLE_IPS=$((USABLE_IPS - USED_IPS))

    # Output the results for the current subnet
    echo "Subnet ID: $SUBNET_ID"
    echo "CIDR Block: $CIDR_BLOCK"
    echo "Total Usable IPs: $USABLE_IPS"
    echo "Used IPs: $USED_IPS"
    echo "Available IPs: $AVAILABLE_IPS"
    echo "-----------------------------------"
done
