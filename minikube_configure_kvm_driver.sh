#!/usr/bin/env bash

curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2 &&
  sudo install docker-machine-driver-kvm2 /usr/local/bin/

minikube config set driver kvm2
minikube config set memory 4096
