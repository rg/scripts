#!/usr/bin/env bash
set -e

DESTINATION_FOLDER="${1:-.}"
export PATH=$DESTINATION_FOLDER:$PATH

if ! command -v cli53 &>/dev/null; then
    DL=$(curl -sL https://api.github.com/repos/barnybug/cli53/releases/latest | jq -r '.assets[]|select(.name=="cli53-linux-amd64")|.browser_download_url')
    curl -sL -o "$DESTINATION_FOLDER/cli53" "$DL"
    chmod +x "$DESTINATION_FOLDER/cli53"
fi

if ! cli53 list; then
    echo "Couldn't list zones to test cli53, failed setup or missing credentials ?"
    exit 1
fi
