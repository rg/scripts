#!/usr/bin/env bash
set -euo pipefail

curl -sL "https://github.com/mr-karan/doggo/releases/download/v0.3.7/doggo_0.3.7_linux_amd64.tar.gz" | tar -x doggo -z
sudo mv doggo /usr/local/bin
doggo
