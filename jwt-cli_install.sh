#!/usr/bin/env bash
set -xe

URL="https://github.com/$(curl -s https://github.com/mike-engel/jwt-cli/releases | grep download | grep linux.tar.gz | head -n1 | cut -d'"' -f2)"
curl -sL $URL | sudo tar -xz -C /usr/local/bin 
jwt --version
