#!/usr/bin/env bash

folder=certificats

echo ".crt"
for f in $(find $folder -name "*.crt" -type f); do
  openssl x509 -in "$f" -text -noout >"$f.txt"
done

echo ".jks"
for f in $(find $folder -name "*.jks" -type f); do
  # http://stackoverflow.com/questions/10103657/how-to-print-the-public-key-of-a-certificate-using-keytool#23302895
  keytool -printcert -file "$f" >"$f.txt"
done
