#!/bin/bash

# # Include with
# DIR="$(cd "$( dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"
# source "$DIR/lib.sh"

set -euo pipefail

function colors.echo_red {
  flags=${2:-''}
  echo -e $flags "\033[31m"$1"\033[0m"
}
function colors.echo_yellow {
  flags=${2:-''}
  echo -e $flags "\033[33m"$1"\033[0m"
}
function colors.echo_blue {
  flags=${2:-''}
  echo -e $flags "\033[34m"$1"\033[0m"
}
function colors.echo_green {
  flags=${2:-''}
  echo -e $flags "\033[32m"$1"\033[0m"
}
function colors.echo_purple {
  flags=${2:-''}
  echo -e $flags "\033[35m"$1"\033[0m"
}
