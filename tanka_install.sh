#!/usr/bin/env bash

sudo curl -Lo /usr/local/bin/tk https://github.com/grafana/tanka/releases/latest/download/tk-linux-amd64
sudo chmod +x /usr/local/bin/tk
