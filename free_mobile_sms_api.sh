#!/bin/bash

# Debug
# echo $* >> /var/log/free_mobile_sms_api.log

# Expecting something like {"user":"123456789","pass":"12xxXX223xxxXXx"}
# You'll get this ID / password on your Free Mobile personal space, after enabling this option
# It should be specified in your Zabbix user profile, Media tab, Send to field with the Media type calling this script
# echo $1 is about removing surrounding quotes added by Zabbix
send_to=$(echo $1)
shift 1

# Any following parameters is the message
message=$(echo $* | xxd -plain | tr -d '\n' | sed 's/\(..\)/%\1/g')
curl -H "Content-Type: application/json" -d $send_to https://smsapi.free-mobile.fr/sendmsg\?msg\=$message

exit $?
