import jwt
import requests
import time
import typer

app = typer.Typer()

def generate_jwt(app_id: str, private_key: str) -> str:
    now = int(time.time())
    payload = {
        'iat': now,  # issued at time
        'exp': now + 60 * 10,  # expiration time (10 minutes)
        'iss': app_id,  # GitHub App ID
    }
    return jwt.encode(payload, private_key, algorithm='RS256')

def get_installation_access_token(installation_id: str, jwt_token: str) -> str:
    url = f'https://api.github.com/app/installations/{installation_id}/access_tokens'
    headers = {
        'Authorization': f'Bearer {jwt_token}',
        'Accept': 'application/vnd.github.v3+json',
    }
    response = requests.post(url, headers=headers)
    
    if response.status_code == 201:
        return response.json()['token']  # Return the access token
    else:
        raise Exception(f"Error getting access token: {response.status_code} {response.text}")

@app.command()
def main(
    app_id: str = typer.Option(..., help="Your GitHub App ID"),
    installation_id: str = typer.Option(..., help="Your GitHub App installation ID"),
    private_key_path: str = typer.Option(..., help="Path to your private key file")
):
    # Load your private key
    with open(private_key_path, 'r') as key_file:
        private_key = key_file.read()

    # Generate the JWT
    jwt_token = generate_jwt(app_id, private_key)
    print("Generated JWT:", jwt_token)

    # Get the installation access token
    try:
        access_token = get_installation_access_token(installation_id, jwt_token)
        print("Installation Access Token:", access_token)
    except Exception as e:
        print(e)

if __name__ == '__main__':
    app()
