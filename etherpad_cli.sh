#!/usr/bin/env bash

API=http://127.0.0.1:9001/api/1.2.13
APIKEY=$(cat /var/lib/etherpad/APIKEY.txt)

PADS=$(curl -s "$API/listAllPads?apikey=$APIKEY" | jq -r '.data.padIDs[]')

COMMAND=${1:-list}

if [ "$COMMAND" = 'list' ]; then
  echo "$PADS"
fi

if [ "$COMMAND" = 'export' ]; then
  rm -rf pads
  mkdir -p pads
  for p in $PADS; do
    curl -s "$API/getText?apikey=$APIKEY&padID=$p" | jq -r '.data.text' >"pads/$p"
  done
  zip -q -r pads.zip pads/
  echo "$(find pads -type f | wc -l) pads exported"
fi

if [ "$COMMAND" = 'import' ]; then
  if [ ! -f pads.zip ]; then
    echo "File ./pads.zip doesn't exist"
    exit 1
  fi
  rm -rf pads
  unzip -q pads.zip

  for p in pads/*; do
    PAD_ID=${p#pads/}
    RESULT=$(curl -s --get --data-urlencode "@$p" "$API/createPad?apikey=$APIKEY&padID=$PAD_ID")
    RC=$(echo "$RESULT" | jq -r '.code')
    MESSAGE=$(echo "$RESULT" | jq -r '.message')
    if [ "$RC" = "0" ]; then
      echo "$PAD_ID created"
    else
      echo "$PAD_ID creation failed: $MESSAGE"
    fi
  done
fi

if [ "$COMMAND" = 'delete' ]; then
  for p in $PADS; do
    RC=$(curl -s "$API/deletePad?apikey=$APIKEY&padID=$p" | jq -r '.code')
    if [ "$RC" = "0" ]; then
      echo "$p deleted"
    else
      echo "$p deletion failed"
    fi
  done
fi
