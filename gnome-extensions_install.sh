#!/usr/bin/env bash

for ext in sound-output-device-chooserkgshank.net.v40.shell-extension.zip bluetooth-quick-connectbjarosze.gmail.com.v26.shell-extension.zip wintilenowsci.com.v7.shell-extension.zip VitalsCoreCoding.com.v52.shell-extension.zip; do

    echo $ext
    wget -q "https://extensions.gnome.org/extension-data/$ext"
    gnome-extensions install --force "./$ext"
    rm "./$ext"

done

echo

gnome-extensions list

echo
echo Enable the extensions manually with "gnome-extensions enable <UUID from the list>"
